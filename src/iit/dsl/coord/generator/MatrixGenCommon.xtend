package iit.dsl.coord.generator

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Variable
import iit.dsl.coord.coordTransDsl.Constant
import iit.dsl.coord.generator.maxima.converter.Utils.TransformConversionInfo

import iit.dsl.maxdsl.maximaDsl.Expression
import iit.dsl.maxdsl.generator.Identifiers

import java.util.ArrayList
import java.util.Iterator
import java.util.List
import org.eclipse.xtend2.lib.StringConcatenation

/**
 * General code generator for coordinate transform matrices.
 *
 * The generated-code model assumes the separation between initialization
 * and update of the matrix. In the initialization the constant coefficients
 * of the matrix are assigned, while the update expects some input and
 * computes the other coefficients.
 */
class MatrixGenCommon
{
    /**
     * Configuration information required by the code generator.
     *
     * Configurability pertains the syntactical details which depend on the
     * target language. Thus, this interface enables the reuse of the core
     * generator for different languages.
     */
    public static interface IConfigurator
    {
        /** The expression that assigns the given value to the given element of the matrix */
        def String matrixAssignment(int row, int col, String valueExpression)

        /** A single line comment in the generated code */
        def String singleLineComment(String comment)

        /** Definition of a local variable holding the sine of the given value */
        def String sinLocalVarDefinition(Variable v, String valueExpression)

        /** Definition of a local variable holding the cosine of the given value */
        def String cosLocalVarDefinition(Variable v, String valueExpression)

        /** The identifier of the local variable holding the sine of the given Variable */
        def String sinLocalVarIdentifier(Variable v)

        /** The identifier of the local variable holding the cosine of the given Variable */
        def String cosLocalVarIdentifier(Variable v)
    }

    public new(IConfigurator config) {
        this.config = config
    }


    /**
     * Generates the assignments for the constant elements of the given matrix.
     *
     * The generated code assumes that the matrix type exposes the
     * 'operator(int,int)' for write-access to the coefficients.
     *
     * \param info Data/metadata related to the matrix
     * \param constants The list of all and only the Constant(s) appearing in
     *        the matrix
     * \return The textwith the code of all the assignments for the constant
     *         elements of the matrix
     */
    def public StringConcatenation matrixInitCode(
        ProcessedMatrixInfo info,
        List<Constant> constants,
        MaximaDSLReplacements replaceSpecs)
    {
        val code   = new StringConcatenation();

        // Get the expressions model for the single given transform
        var Iterator<Expression> constExprIterator = new ArrayList<Expression>().iterator
        val maximaDSLDoc = MaximaDSLUtils::constantsDocument(info, constants)
        if(maximaDSLDoc.length != 0) {
            val constantsModel    = maximaDSL.getParsedTextModel(maximaDSLDoc.toString())
            constExprIterator = constantsModel.expressions.iterator
        }

        var r = 0    // row index
        var c = 0    // column index
        for(row : info.asText) {
            for(el : row) {
                var rhs = ""
                var com = ""
                if( info.floatLiterals.get(r).get(c) )
                {
                    // The item is a _numerical_ constant, i.e. a float literal.
                    // Thus it will appear as it is in the generated code
                    rhs = el
                } else if( info.constants.get(r).get(c) )
                {
                    // The item is an identifier that represents a constant.
                    // We assume as many parsed expressions as the number of constant elements of the matrix
                    if( ! constExprIterator.hasNext()) {
                        throw new RuntimeException("Expecting more parsed Maxima expressions")
                    }
                    rhs = Identifiers.instance.toCode(constExprIterator.next(), replaceSpecs)
                    com = el
                }
                if(rhs.length > 0) {
                    code.append( config.matrixAssignment(r, c, rhs) )
                    if(com.length > 0) {
                        code.append("    ")
                        code.append( config.singleLineComment( "Maxima DSL: " + com ) )
                    }
                    code.newLine()
                }
                c = c+1
            }
            r = r+1 // next row
            c = 0   // back to first column
        }
        return code
    }

    def public matrixUpdateCode(
        Model model,
        TransformConversionInfo info,
        MaximaDSLReplacements replaceSpecs)
    {
        if(info.maximaDSLText.length == 0) return ''''''
        val StringConcatenation strBuff = new StringConcatenation();

        val varAccess = replaceSpecs.variablesAccess

        for( vari : info.basic.variables ) {
            if( vari.isRotation ) {
                val valueExpr = varAccess.valueExpression(model, vari.variable)
                strBuff.append( config.sinLocalVarDefinition(vari.variable, valueExpr) )// '''double «sineVarName  (vari.variable)» = std::sin( «valueExpr» );''' + "\n")
                strBuff.newLine()
                strBuff.append( config.cosLocalVarDefinition(vari.variable, valueExpr) )// '''double «cosineVarName(vari.variable)» = std::cos( «valueExpr» );''' + "\n")
                strBuff.newLine()
            }
        }
        //strBuff.append("/* DEBUG info : MaximaDSL snippet \n");
        //strBuff.append(info.maximaDSLText);
        //strBuff.append("*/\n");

        val maximamodel = maximaDSL.getParsedTextModel(info.maximaDSLText.toString)
        val exprs = maximamodel.expressions.iterator
        var r = 0 // row index
        var c = 0 // column index
        for(row : info.layers.asText) {
            for(el : row) {
                if( ! info.layers.constants.get(r).get(c) )
                {
                    // we assume as many parsed expressions as the number of non constant elements of the matrix
                    if( ! exprs.hasNext()) {
                        throw new RuntimeException("Expecting more parsed Maxima expressions")
                    }
                    val expr = exprs.next()
                    //strBuff.append( '''// MaximaDSL: «NodeModelUtils.findActualNodeFor(expr).text.replace("\n","")»''')
                    //strBuff.newLine()
                    strBuff.append( config.matrixAssignment(r,c, exprGen.toCode(expr, replaceSpecs)) )
                    strBuff.newLine()
                }
                c = c+1
            }
            r = r+1 // next row
            c = 0   // back to first column
        }
        return strBuff
    }

    def public MaximaDSLReplacements
        makeMaximaDSLReplacements(Model m, IVariablesAccess vars,
                               IParametersAccess pars, IConstantsAccess consts)
    {
        return new MaximaDSLReplacements(m, new VarsAccess(vars, this.config), pars, consts)
    }

    /**
     * Decorator of IVariablesAccess overriding the implementation of
     * valueExpression(Model model, Variable v, String function)
     * to make sure it complies with the rest of the code as generated by the
     * methods in this package.
     */
    public static class VarsAccess implements IVariablesAccess
    {
        public new(IVariablesAccess decorateMe, IConfigurator cfg) {
            decorated = decorateMe
            config = cfg
        }
        override valueExpression(Model model, Variable arg) {
            return decorated.valueExpression(model, arg)
        }

        /**
         * This is the only one we need to override, to enforce the policy of
         * this package: generated code for matrices uses local variables to
         * store the value of sin/cos of variables.
         */
        override valueExpression(Model model, Variable v, String function)
        {
            return switch function {
                case "sine"  : config.sinLocalVarIdentifier(v)
                case "cosine": config.cosLocalVarIdentifier(v)
                default : throw new RuntimeException("unknown function " + function)
            }
        }

        private IVariablesAccess decorated
        private IConfigurator config
    }


    private IConfigurator config = null
    private static iit.dsl.maxdsl.utils.DSLAccessor   maximaDSL = new iit.dsl.maxdsl.utils.DSLAccessor()
    private static iit.dsl.maxdsl.generator.Identifiers exprGen = iit.dsl.maxdsl.generator.Identifiers.instance
}
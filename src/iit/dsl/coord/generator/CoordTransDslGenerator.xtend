package iit.dsl.coord.generator

import iit.dsl.coord.coordTransDsl.Model

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IGenerator2
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class CoordTransDslGenerator implements IGenerator2
{
    extension Common utils = new Common()

    override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context)
    {
        val model = resource.contents.head as Model;

        val maximaGen = new iit.dsl.coord.generator.maxima.Generator()
        val matlabGen = new iit.dsl.coord.generator.matlab.Generator()
        val cppGen    = new iit.dsl.coord.generator.cpp.Generator()

        maximaGen.doGenerate(resource, fsa)
        matlabGen.doGenerate(resource, fsa)
        cppGen   .doGenerate(resource, fsa)

        testParamsAndVars(model)
    }

    override afterGenerate(Resource input, IFileSystemAccess2 fsa, IGeneratorContext context) {
    }

    override beforeGenerate(Resource input, IFileSystemAccess2 fsa, IGeneratorContext context) {
    }

    def testParamsAndVars(Model model) {
        for(t : model.transforms) {
            System::out.println("\n ** transform: " + t.name)
            System::out.println("Does it depend on some parameters? " + Common::dependsOnParameters(model) + "\n")
            System::out.println("variables : " + t.argsListText)
            System::out.println("parameters : " +t.paramsListText)
            for(p : t.params) {
                System::out.println(p.name + " in group " + Common::getGroup(p).name)
            }
        }
	}

}

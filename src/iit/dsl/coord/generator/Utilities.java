package iit.dsl.coord.generator;

public abstract class Utilities {
	/**
	 * Enumerates the two possible conventions for transformation matrices.
	 * Consider frame A and B, with B moved with respect to A (say with a
	 * rotation of alpha radians about the z axis).
	 * The corresponding rotation matrix can take two forms: either it takes
	 * a point of B (the rotated frame) and gives back its coordinates
	 * expressed in A, or the other way round.
	 * Assuming the matrix is always left multiplied with a column vector of
	 * coordinates, we can call the two matrices A_X_B and B_X_A respectively,
	 * so that it is clear which is the frame in which the vector should be
	 * expressed.
	 * @author Marco Frigerio
	 *
	 */
	public enum MatrixConvention {

		TRANSFORMED_FRAME_ON_THE_RIGHT("right"),///< matrices are in the form A_X_B
		TRANSFORMED_FRAME_ON_THE_LEFT("left");  ///< matrices are in the form B_X_A

		private final String docKeyword;
		private MatrixConvention(String key) { docKeyword = key; }
		/**
		 * Return the keyword of the Coordinate Transforms DSL that corresponds
		 * to this instance
		 */
	    public String getDocKeyword() { return docKeyword; }
	}

    public enum MatrixType {
        _6D(6), _6D_FORCE(6), HOMOGENEOUS(4);

        private final int size;
        private MatrixType(int s) { size = s; }
        public int size() { return size; }
    };

    public static boolean isInteger(float num) {
        return num == (int)num;
    }
    public static int asInteger(float num) {
        return (int)num;
    }

}

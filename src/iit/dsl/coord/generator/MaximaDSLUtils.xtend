package iit.dsl.coord.generator

import iit.dsl.coord.coordTransDsl.Transform
import iit.dsl.coord.coordTransDsl.Constant
import iit.dsl.coord.generator.maxima.Maxima
import iit.dsl.coord.generator.maxima.converter.Constants

import org.eclipse.xtend2.lib.StringConcatenation
import java.util.List
import java.util.Set

class MaximaDSLUtils
{

    @Deprecated
    def public static MaximaDSLDocumentText(Transform T, String[][] asText) '''
        «val vars   = common.getVars(T)»
        «val params = common.getParams(T)»
        «val consts = common.getConstants(T)»
        «IF (vars.size != 0  ||  params.size != 0)»
            Variables {
                «common.argsListText(vars)»
                «IF vars.size > 0 && params.size > 0»,«ENDIF»
                «FOR p : params SEPARATOR ", "»«Maxima::parameterToMaximaVarName(p)»«ENDFOR»
                «IF (vars.size>0 || params.size>0) && consts.size > 0»,«ENDIF»
                «FOR c : consts SEPARATOR ", "»«Constants.toMaximaIdentifier(c)»«ENDFOR»
            }

            «FOR row : asText»
                «FOR el : row»
                    «IF (!iit::dsl::maxdsl::utils::MaximaConversionUtils::isConstant(el))»
                        «el»;
                    «ENDIF»
                «ENDFOR»
            «ENDFOR»
        «ENDIF»
     '''

     def public static MaximaDSLDocumentText(Set<String> varnames, ProcessedMatrixInfo info)
     {
        val doc = new StringConcatenation();

        if (varnames.size > 0 ) {
            doc.append('''
            Variables {
                «FOR name : varnames SEPARATOR", "»«name»«ENDFOR»
            }
            ''')
        }
        val preambleLength = doc.length
        val text = info.asText
        val rows = text.length
        val cols = text.get(0).length
        for( var r=0; r<rows; r++ ) {
            for( var c=0; c<cols; c++ ) {
                if( ! info.constants.get(r).get(c) ) {
                    doc.append(text.get(r).get(c) + ";\n")
                }
            }
        }
        // check if any expression was actually generated; if not, everything is constant
        if( doc.length > preambleLength) {
            return doc
        }
        return ""
     }

    /**
     * A document of the MaximaDSL containing only expressions involving symbolic
     * constants (ie not float literals), for the given matrix.
     */
    def public static constantsDocument(ProcessedMatrixInfo info, List<Constant> constants)
    {
        val preamble = new StringConcatenation()
        val body     = new StringConcatenation()
        val asText = info.asText
        val rows   = asText.length
        val cols   = asText.get(0).length
        if( constants.length > 0 )
        {
            preamble.append('''
            Variables {
                «FOR c : constants SEPARATOR ", "»«Constants.toMaximaIdentifier(c)»«ENDFOR»
            }
            ''')

            for( var r=0; r<rows; r++ ) {
                for( var c=0; c<cols; c++ ) {
                    if( info.constants.get(r).get(c) && !info.floatLiterals.get(r).get(c)) {
                        body.append(asText.get(r).get(c) + ";\n")
                    }
                }
            }
        }
        if(body.length > 0) {
            preamble.append( body )
            return preamble
        }
        return ''''''
    }


    private static Common common = new Common()

}
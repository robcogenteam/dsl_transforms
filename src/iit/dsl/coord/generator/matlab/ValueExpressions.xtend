package iit.dsl.coord.generator.matlab

import iit.dsl.coord.generator.IVariablesAccess
import iit.dsl.coord.generator.IParametersAccess
import iit.dsl.coord.generator.IConstantsAccess

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Variable
import iit.dsl.coord.coordTransDsl.ParameterLiteral
import iit.dsl.coord.coordTransDsl.Constant
import java.util.Map

class VectorBasedVarsAccess implements IVariablesAccess
{
    public new(String vec, Map<String,Integer> varToIndex) {
        vectorName = vec
        varNameToIndex = varToIndex
    }


    override valueExpression(Model model, Variable arg) {
        return vectorName + "(" + varNameToIndex.get(arg.varname) + ")"
    }

    override valueExpression(Model model, Variable p, String function) {
        val f = switch function {
            case "sine"  : "sin"
            case "cosine": "cos"
            default : "%%ERROR, unknown function " + function
        }
        return f + "(" + valueExpression(model, p) + ")"
    }

    private final String vectorName;
    private final Map<String,Integer> varNameToIndex
}


class StructBasedParsAccess implements IParametersAccess
{
    public new(String params) {
        paramsStructName = params
    }

    override valueExpression(Model model, ParameterLiteral p) {
        return paramsStructName + "." + p.name
    }

    override valueExpression(Model model, ParameterLiteral p, String function) {
        val f = switch function {
            case "sine"  : "sin"
            case "cosine": "cos"
            default : "%%ERROR, unknown function " + function
        }
        return f + "(" + valueExpression(model, p) + ")"
    }

    private final String paramsStructName;
}



class StructBasedConstsAccess implements IConstantsAccess
{
    public new(String struct) {
        constsStructName = struct
    }

    def public String structName() { return constsStructName }

    override valueExpression(Model model, Constant p) {
        return constsStructName + "." + p.name
    }

    override valueExpression(Model model, Constant p, String function) {
        val f = switch function {
            case "sine"  : "sin"
            case "cosine": "cos"
            default : "%%ERROR, unknown function " + function
        }
        return f + "(" + valueExpression(model, p) + ")"
    }

    private final String constsStructName;
}


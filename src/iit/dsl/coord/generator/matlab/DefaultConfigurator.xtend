package iit.dsl.coord.generator.matlab

import iit.dsl.coord.coordTransDsl.Model

import java.util.Arrays
import java.util.HashMap

import iit.dsl.coord.generator.IConstantsAccess
import iit.dsl.coord.generator.Common


class DefaultConfigurator implements IConfigurator
{
    new()
    {}

    override getMaximaConverterConfigurator() {
        return maximaConverterConfigurator
    }

    override getConstantsValueExprGenerator(Model model) {
        return constsAccess
    }

    override getVariablesValueExprGenerator(Model model) {
        val vars = Common.getInstance().getVars(model)
        val map = new HashMap<String,Integer>()
        var i = 0
        for( v : vars ) {
            map.put(v.varname, i)
            i = i+1
        }
        return new VectorBasedVarsAccess(varsVectorName, map)
    }

    override getUpdateFunctionArguments() {
        return Arrays::asList(updateFArgs)
    }

    override getInitFunctionArguments() {
        return #[constsStructName]
    }

    private iit.dsl.coord.generator.maxima.converter.IConfigurator maximaConverterConfigurator =
        new iit.dsl.coord.generator.maxima.converter.DefaultConfigurator()

    private String[] updateFArgs = #[varsVectorName, paramsStructName, constsStructName]

    private IConstantsAccess constsAccess = new StructBasedConstsAccess(constsStructName)

    protected static String varsVectorName   = "status"
    protected static String paramsStructName = "params"
    protected static String constsStructName = "consts"


}
package iit.dsl.coord.generator.matlab

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.IFileSystemAccess


import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Transform
import iit.dsl.coord.generator.Utilities
import iit.dsl.coord.generator.Common

import iit.dsl.coord.coordTransDsl.ParameterLiteral



class Generator implements IGenerator
{
    override void doGenerate(Resource resource, IFileSystemAccess fsa) {
        val model = resource.contents.head as Model;
        generateHomogeneousTransformsFiles(model, fsa)
        generateSpatialTransformsFiles(model, fsa)
    }

    /**
     * The Matlab identifier that will appear in the generated code to
     * represent the given transform
     */
    def public static identifier(Transform t, Utilities$MatrixType type) {
        var String token = "";
        switch(type) {
            case Utilities$MatrixType::HOMOGENEOUS:
                token = "Xh"
            case Utilities$MatrixType::_6D:
                token = "XM"
            case Utilities$MatrixType::_6D_FORCE:
                token = "XF"
        }
        return t.leftFrame.name + "_" + token + "_" + t.rightFrame.name
    }

    def public static parameterValueAccessor(ParameterLiteral param)
        '''«Common::getGroup(param).name».«param.name»'''

    /**
     * Converts a MatrixType to the default name associated with it
     */
    def public static toName(Utilities$MatrixType type)
    {
        switch(type) {
            case Utilities$MatrixType::HOMOGENEOUS:
                return "homogeneous"
            case Utilities$MatrixType::_6D:
                return "motion"
            case Utilities$MatrixType::_6D_FORCE:
                return "force"
        }
    }

    /**
     * The name of the function that initializes the transforms of the given type
     */
    def public static initFunctionName(Utilities$MatrixType type)
    {
        val typeName    = toName(type)
        val typeNameCap = Character.toUpperCase(typeName.charAt(0)) + typeName.substring(1)
        return '''init«typeNameCap»Transforms'''
    }
    /**
     * The name of the function that updates the transforms of the given type
     */
    def public static updateFunctionName(Utilities$MatrixType type)
    {
        val typeName    = toName(type)
        val typeNameCap = Character.toUpperCase(typeName.charAt(0)) + typeName.substring(1)
        return '''update«typeNameCap»Transforms'''
    }


    public new()
    {
        this(new DefaultConfigurator())
    }

    public new(IConfigurator configurator)
    {
        actualGenerator = new GenerationCore(configurator)
    }


    /**
     * \name Files generators
     * Generate two files with the code for the initialization and the update of the
     * coordinate transforms specified in the given model.
     */
    ///@{
    def generateHomogeneousTransformsFiles(Model transforms, IFileSystemAccess fsa)
    {
        actualGenerator.setModel( transforms )
        val mxtype = Utilities$MatrixType::HOMOGENEOUS

        fsa.generateFile(
            transforms.name.toLowerCase() + "/" + initFunctionName(mxtype) + ".m",
            actualGenerator.init_fileContent(mxtype)
        );
        fsa.generateFile(
            transforms.name.toLowerCase() + "/" + updateFunctionName(mxtype) + ".m",
            actualGenerator.update_fileContent(mxtype)
        );
    }

    def generateSpatialTransformsFiles(Model transforms, IFileSystemAccess fsa)
    {
        actualGenerator.setModel( transforms )
        var mxtype = Utilities$MatrixType::_6D
        val prefix = transforms.name.toLowerCase() + "/"

        fsa.generateFile( prefix + initFunctionName(mxtype) + ".m",
            actualGenerator.init_fileContent(mxtype)
        );
        fsa.generateFile(prefix + updateFunctionName(mxtype) + ".m",
            actualGenerator.update_fileContent(mxtype)
        );

        mxtype = Utilities$MatrixType::_6D_FORCE
        fsa.generateFile( prefix + initFunctionName(mxtype) + ".m",
            actualGenerator.init_fileContent(mxtype)
        );
        fsa.generateFile(prefix + updateFunctionName(mxtype) + ".m",
            actualGenerator.update_fileContent(mxtype)
        );
    }
    ///@}

    private GenerationCore actualGenerator = null

}
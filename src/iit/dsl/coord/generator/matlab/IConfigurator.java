package iit.dsl.coord.generator.matlab;

import java.util.List;

import iit.dsl.coord.coordTransDsl.Model;
import iit.dsl.coord.generator.IConstantsAccess;
import iit.dsl.coord.generator.IVariablesAccess;



/**
 * Configuration information required by the Matlab code generator of this package.
 *
 * @author Marco Frigerio
 */
public interface IConfigurator
{
    /**
     * @return the configurator object to be used with the MaximaConverter
     */
    iit.dsl.coord.generator.maxima.converter.IConfigurator
                                              getMaximaConverterConfigurator();

    IVariablesAccess  getVariablesValueExprGenerator(Model model);
    IConstantsAccess  getConstantsValueExprGenerator(Model model);

    /**
     * @return the list of arguments that will appear in the signature of the
     *         Matlab function that updates the content of the coordinate
     *         transforms.
     *
     * These arguments will possibly appear along with other ones, which are
     * required regardless of the implementation of this interface.
     *
     * This function shall be implemented consistently with the implementation
     * of IVariablesAccess and IConstantsAccess, so that the identifiers
     * used in the function body match the formal parameters.
     */
    List<String> getUpdateFunctionArguments();

    List<String> getInitFunctionArguments();
}

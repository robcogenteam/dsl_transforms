package iit.dsl.coord.generator.matlab

import java.util.List
import java.util.ArrayList
import org.eclipse.xtend2.lib.StringConcatenation

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Variable
import iit.dsl.coord.coordTransDsl.Transform

import iit.dsl.coord.generator.maxima.converter.MaximaConverter
import iit.dsl.coord.generator.maxima.converter.Utils.TransformConversionInfo
import iit.dsl.coord.generator.maxima.converter.Utils

import iit.dsl.coord.generator.Utilities
import iit.dsl.coord.generator.TransformsInfo
import iit.dsl.coord.generator.MatrixGenCommon
import iit.dsl.coord.generator.MaximaDSLReplacements

/**
 * The actual implementation of the Matlab code generation
 */
class GenerationCore
{
    public new(IConfigurator config)
    {
        configurator    = config
        maximaConverter = new MaximaConverter( configurator.getMaximaConverterConfigurator() )
        maximaConverter.setMode(MaximaConverter$Mode::TRIGSIMP);
        currModel = null
        currType  = null
    }

    def public setModel(Model model) {
        currModel = model
        currType  = null
        maximaReplSpecs = helper.makeMaximaDSLReplacements(
                currModel,
                configurator.getVariablesValueExprGenerator(currModel),
                new StructBasedParsAccess("params"),
                configurator.getConstantsValueExprGenerator(currModel) )
    }

    /**
     * The Matlab code with the initialization of all the transforms specified
     * in the given model.
     * It is basically just the assignments of the constant elements of the
     * matrices.
     */
    def public CharSequence init_fileContent(Utilities$MatrixType type)
    {
        load(type)
        return initFunctionSignature() + "\n\n" + init_code()
    }

    /**
     * The content of the file with the Matlab function to update the coordinate
     * transforms.
     * @see update_functionBody
     */
    def public CharSequence update_fileContent(Utilities$MatrixType type)
    {
        load(type)
        return  updateFunctionSignature() + "\n\n" + update_code() + "\n\n" +
                "out = " + transformsStructName + ";"
    }

    def private load(Utilities$MatrixType type)
    {
        if(currModel === null) {
            throw new RuntimeException("Cannot generate code if a Model was not previously set")
        }
        if( ! type.equals( currType) ) {
            currType = type
            tinfo = Utils.getConversionInfo(currModel,
                             TransformsInfo.generalInfo(currModel),
                             currType, maximaConverter)
        }
    }

    def public getTransformsLocalVarName() { return transformsStructName }
    def public setTransformsLocalVarName(String varname) {
        transformsStructName = varname
    }



    def private initFunctionSignature()
        '''function «transformsStructName» = «Generator::initFunctionName(currType)»(«initFunctionArgsList»)'''

    def private updateFunctionSignature()
        '''function out = «Generator::updateFunctionName(currType)»(«updateFunctionArgsList()»)'''

    def private initFunctionArgsList() {
        val string = new StringConcatenation()
        val iter = configurator.initFunctionArguments.iterator
        while( iter.hasNext ) {
            string.append( iter.next )
            if( iter.hasNext ) {
                string.append(", ")
            }
        }
        return string
    }

    def private updateFunctionArgsList()
    {
        var StringConcatenation ret = new StringConcatenation()
        ret.append(transformsStructName)
        for(arg : configurator.getUpdateFunctionArguments() ) {
            ret.append(", " + arg)
        }
        return ret
    }

    def private init_code()
    {
        val code = new StringConcatenation()
        for( info : tinfo )
        {
            val id = mxId(info.basic.transform );
            code.append( id + " = zeros(" + currType.size + "," + currType.size + ");" )
            code.newLine()
            helperCfg.setCurrentMatrixName( id )
            code.append( helper.matrixInitCode(
               info.layers, info.basic.constants, maximaReplSpecs) )
            code.newLine()
        }
        return code
    }

    def private update_code()
    {
        val code = new StringConcatenation()
        for( info : tinfo ) {
            helperCfg.setCurrentMatrixName( mxId(info.basic.transform ) )
            code.append( helper.matrixUpdateCode( currModel, info, maximaReplSpecs ) )
            code.newLine()
        }
        return code
    }

    def private mxId(Transform t) {
        return transformsStructName + "." + Generator::identifier(t, currType)
    }



    private CharSequence transformsStructName = '''tr'''

    private IConfigurator   configurator = null;
    private MaximaConverter maximaConverter = null
    private CommonGenConfig helperCfg = new CommonGenConfig
    private MatrixGenCommon helper    = new MatrixGenCommon( helperCfg )

    private MaximaDSLReplacements maximaReplSpecs = null

    private List<TransformConversionInfo> tinfo= new ArrayList<TransformConversionInfo>()

    private Model currModel = null
    private Utilities$MatrixType currType = null



    public static class CommonGenConfig implements MatrixGenCommon.IConfigurator
    {
        def public setCurrentMatrixName(String name) {
            matrixName = name
        }

        override matrixAssignment(int row, int col, String valueExpression) {
            val Integer r = row + 1
            val Integer c = col + 1
            return matrixName + "(" + r + "," + c + ") = " + valueExpression + ";"
        }

        override singleLineComment(String comment) {
            return "% " + comment
        }

        override sinLocalVarDefinition(Variable v, String valueExpression) {
            return sinLocalVarName(v) + " = sin( " + valueExpression + ");"
        }

        override cosLocalVarDefinition(Variable v, String valueExpression) {
            return cosLocalVarName(v) + " = cos( " + valueExpression + ");"
        }

        override sinLocalVarIdentifier(Variable v) {
            return sinLocalVarName(v)
        }

        override cosLocalVarIdentifier(Variable v) {
            return cosLocalVarName(v)
        }

        private String matrixName = "varstate"

        def public static String sinLocalVarName(Variable arg) {
           return "sin_" + arg.varname
        }
        def public static String cosLocalVarName(Variable arg) {
            return "cos_" + arg.varname
        }
    }
}
package iit.dsl.coord.generator

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.ParameterLiteral
import iit.dsl.coord.coordTransDsl.Constant
import iit.dsl.coord.coordTransDsl.Variable

/**
 * Configuration interface for code generation from a transforms Model
 */
public interface IVariablesAccess
{
    /**
     * Converts a Variable into a valid expression in the target language.
     *
     * Such expression will be used in the generated code whenever the value of
     * the Variable is needed.
     * @param model the transforms Model, ie the current "context"
     * @param v the variable to be converted
     * @return a valid expression in the target language that shall evaluate
     *        to the actual value of the variable
     */
    def public String valueExpression(Model model, Variable v);

    /**
     * The expression that shall resolve to the value of the given function
     * applied to the given variable.
     * Such expression will be inserted in the generated code.
     *
     * This function allows to customize how symbolic functions such as the sine
     * of a variable should be transformed into valid code.
     * @param model
     * @param v
     * @param function currently must be either "sine" or "cosine"
     * @return
     */
    def public String valueExpression(Model model, Variable v, String function);
}

/**
 * Configuration interface for code generation from a transforms Model
 */
public interface IParametersAccess
{
    /**
     * The expression that shall resolve to the value of the given parameter,
     * in the generated code.
     *
     * @param model the transforms Model, ie the current "context"
     * @param p the parameter
     * @return A string representing a valid expression in the target language,
     *         that shall resolve to the value of the given parameter
     */
    def public String valueExpression(Model model, ParameterLiteral p);

    /**
     * The expression that shall resolve to the value of the given function
     * applied to the given parameter.
     * Such expression will be inserted in the generated code.
     *
     * This function allows to customize how symbolic functions such as the sine
     * of a parameter should be transformed into valid code.
     *
     * @param model the transforms Model, ie the current "context"
     * @param p
     * @param function currently must be either "sine" or "cosine"
     * @return
     */
    def public String valueExpression(Model model, ParameterLiteral p, String function);
}

/**
 * Configuration interface for code generation from a transforms Model
 */
public interface IConstantsAccess
{
    /**
     * The expression that shall resolve to the value of the given constant,
     * in the generated code.
     *
     * @param model the transforms Model, ie the current "context"
     * @param c the constant of interest
     * @return A string representing a valid expression in the target language,
     *         that shall resolve to the value of the given constant
     */
    def public String valueExpression(Model model, Constant c);

    /**
     * The expression that shall resolve to the value of the given function
     * applied to the given constant. Such expression will be inserted in the
     * generated code.
     *
     * This function allows to customize how symbolic functions such as the sine
     * of a constant should be transformed into valid code.
     *
     * @param model the transforms Model, ie the current "context"
     * @param c
     * @param function currently must be either "sine" or "cosine"
     * @return
     */
    def public String valueExpression(Model model, Constant c, String function);
}

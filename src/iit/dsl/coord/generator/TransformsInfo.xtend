package iit.dsl.coord.generator

import iit.dsl.coord.coordTransDsl.Constant
import iit.dsl.coord.coordTransDsl.Variable
import iit.dsl.coord.coordTransDsl.ParameterLiteral
import iit.dsl.coord.coordTransDsl.Expr
import iit.dsl.coord.coordTransDsl.CoordTransDslPackage
import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Transform

import iit.dsl.coord.generator.Common


import java.util.List
import java.util.ArrayList
import java.util.HashSet

public class ProcessedMatrixInfo
{
    public new(String[][] text, boolean[][] maskFloats, boolean[][] maskConsts )
    {
        asText        = text
        floatLiterals = maskFloats
        constants     = maskConsts
    }

    public final String [][] asText;
    public final boolean[][] floatLiterals;
    public final boolean[][] constants;
}


class TransformsInfo
{
    public static class VarInfo
    {
        public new (Variable v, boolean rotation)
        {
            variable   = v
            isRotation = rotation
        }
        public final Variable variable
        public final boolean  isRotation
    }


    public static class Arguments
    {
        public new(Transform T)
        {
            transform = T
            setVars(T)
            parameters= transform.params
            constants = transform.constants
        }

        public final Transform   transform
        public final List<VarInfo>          variables = new ArrayList<VarInfo>();
        public final List<ParameterLiteral> parameters;
        public final List<Constant>         constants;

        def public boolean isParametric() { return parameters.size > 0}

        private extension Common common = Common::getInstance()

        def private setVars(Transform T)
        {
            val names = new HashSet<String>(); // to check whether a variable has been already added
            for( mx : T.matrices) {
                if( mx.arg instanceof Expr) {
                    val arg = mx.arg as Expr
                    if( arg.identifier.eClass().getClassifierID() == CoordTransDslPackage.VARIABLE) {
                        val variable = arg.identifier as Variable
                        if( ! names.contains(variable.varname)) {
                            names.add(variable.varname)
                            val rot = CoordTransDslPackage.Literals.ROTATION.isSuperTypeOf( mx.eClass )
                            variables.add( new VarInfo(variable, rot) )
                        }
                    }
                }
            }
        }
    }


    def public static List<TransformsInfo.Arguments> generalInfo(Model model)
    {
        val transforms = model.noDuplicatesTransformsList
        val ret = new ArrayList<TransformsInfo.Arguments>()
        for( t : transforms ) {
            ret.add( new Arguments(t) )
        }
        return ret
    }



    private static extension Common common = Common::getInstance()
}

package iit.dsl.coord.generator.maxima.converter;

import iit.dsl.coord.generator.maxima.converter.MaximaConverter.Mode;

/**
 * Generic configuration interface for MaximaConverter objects.
 * @author Marco Frigerio
 */
public interface IConfigurator
{
    /**
     * The path of the Maxima source code ("libraries") with the required
     * utilities
     */
    public String getLibsPath();
    /**
     * The file name with the Maxima code implementing the basic transforms.
     */
    public String getTransformsLibName();
    /**
     * The path where to find the Maxima code implementing the transforms
     * to be converted by a MaximaConverter instance.
     */
    public String getGeneratedCodeLocation();
    /**
     * The simplification mode to be used during the interpretation of the
     * Maxima transforms.
     * @see Mode
     */
    public String getConversionMode();
    /**
     * The configurator object to be used by the Maxima engine used in turn
     * by a MaximaConverter.
     */
    public iit.dsl.maxdsl.utils.MaximaRunner.IConfigurator getMaximaEngineConfigurator();
}
package iit.dsl.coord.generator.maxima.converter


class DefaultConfigurator implements IConfigurator
{
    override getConversionMode() {
        return MaximaConverter$Mode::TRIGSIMP.asString
    }

    override getGeneratedCodeLocation() {
        return (System::getenv("ROBOTS_MAXIMA_FOLDER") + "/")
    }

    override getLibsPath() {
        return (System::getenv("ROBOGEN_MAXIMALIBS_PATH") + "/")
    }

    override getTransformsLibName() {
        return "transforms"
    }

    override getMaximaEngineConfigurator() {
        return new iit.dsl.maxdsl.utils.MaximaRunner$DefaultConfigurator()
    }
}
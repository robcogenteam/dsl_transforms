package iit.dsl.coord.generator.maxima.converter

import iit.dsl.coord.coordTransDsl.ParameterLiteral
import iit.dsl.coord.generator.Common
import iit.dsl.coord.generator.ParameterFQN

class Parameters
{
    def public static String toMaximaIdentifier(ParameterLiteral param) {
        return prefix + Common.getGroup(param).name + separator + param.name
    }

    def public static ParameterFQN maximaVarToParamName(String varname)
    {
        if(! varname.startsWith(prefix)) return null
        val tmp = varname.split(separator)
        if(tmp.size != 2) {
            throw(new RuntimeException(
            "Could not guess the parameter group and parameter name from the variable name " + varname))
        }
        return new ParameterFQN(tmp.head.replaceFirst(prefix, ""), tmp.last)
    }

    private static String prefix    = "_p__"
    private static String separator = "___"
}
package iit.dsl.coord.generator.maxima.converter;

import java.io.File;
import java.util.List;

import org.eclipse.xtend2.lib.StringConcatenation;

import iit.dsl.coord.coordTransDsl.Constant;
import iit.dsl.coord.coordTransDsl.Model;
import iit.dsl.coord.coordTransDsl.Transform;
import iit.dsl.coord.generator.Common;
import iit.dsl.coord.generator.ProcessedMatrixInfo;
import iit.dsl.coord.generator.Utilities;
import iit.dsl.coord.generator.maxima.converter.DefaultConfigurator;
import iit.dsl.maxdsl.utils.MaximaConversionUtils;
import iit.dsl.coord.generator.maxima.Maxima;

/**
 * Facility to interpret the Maxima expressions corresponding to a
 * Transform.
 *
 * This class is typically used after the Maxima code has been generated,
 * to support the generation of code in other languages, that is,
 * to convert Maxima code into optimized code in other languages. Hence
 * the name 'Converter'.
 *
 * @author Marco Frigerio
 */
public class MaximaConverter
{
    /**
     * The possible modes for the simplification of Maxima expressions
     */
    public static enum Mode {
        PLAIN("plain"),          ///< no simplification function is used
        TRIGSIMP("trigsimp"),    ///< uses 'trigsimp(<expression>)'
        TRIGREDUCE("trigreduce"),///< uses 'float(ratexpand(ratsimp(trigreduce(<expression>))))'
        __UNKNOWN("__unknow__");

        private String asStr;
        private Mode(String str) { asStr = str; }
        public String asString() { return asStr; }
    }


    private IConfigurator configurator = null;
    private Maxima   maxima  = null;
    private boolean modelSet = false;
    private Mode     mode    = Mode.PLAIN;
    private iit.dsl.maxdsl.utils.MaximaRunner maximaRunner = null;

    public MaximaConverter() {
        this(new DefaultConfigurator());
    }

    public MaximaConverter(IConfigurator config) {
        maxima = new Maxima();//TODO there might be the need to use different subclasses, like Maxima6D
        if(config == null) {
            //TODO log warning
            System.err.println("Warning in iit.dsl.coord.generator.MaximaConverter() : null configurator");
            return;
        }
        setConfigurator(config);
    }

    public void setConfigurator(IConfigurator config) {
        configurator = config;
        Mode tmpMode = parseStringMode(config.getConversionMode());
        if(tmpMode == Mode.__UNKNOWN) {
            System.err.print("iit.dsl.coord.generator.MaximaConverter - " +
                    "Warning, unknown conversion mode '"
                    + config.getConversionMode() + "'. Using PLAIN.");
            tmpMode = Mode.PLAIN;
        }
        setMode(tmpMode);
    }

    /**
     * This overwrites the conversion mode given by the IConfigurator
     * @param mod
     */
    public void setMode(Mode mod) {
        this.mode = mod;
    }
    /**
     * The current conversion Mode used by this instance
     */
    public Mode getMode() { return this.mode; }

    /**
     * Configure the transforms Model whose elements (ie the individual
     * Transforms) are to be interpreted.
     *
     * @param model The transforms model.
     * @param type The kind of transforms of interest (spatial motion,
     *   spatial force, homogeneous)
     *
     * This function starts a Maxima interpreter and loads the generated
     * Maxima code for the given transforms.
     */
    public void setModel(Model model, Utilities.MatrixType type)
    {
        maximaRunner = new iit.dsl.maxdsl.utils.MaximaRunner(
                configurator.getMaximaEngineConfigurator());
        maximaRunner.runBatch(configurator.getLibsPath() + "/" +
                configurator.getTransformsLibName());

        String fileName = configurator.getGeneratedCodeLocation() + "/";
        switch(type) {
            case _6D:
                fileName += Maxima.transforms6DFileName(model);
                break;
            case _6D_FORCE:
                fileName += Maxima.transforms6DForceFileName(model);
                break;
            case HOMOGENEOUS:
                fileName += Maxima.transformsFileName(model);
                break;
            default:
                throw(new RuntimeException("Unknown MatrixType: " + type));
        }
        if( !(new File(fileName)).isFile() ) {
            done();
            throw new RuntimeException("Could not find Maxima source file: '" + fileName +
                    "' (did you forget to generate it?)");
        }
        maximaRunner.runBatch(fileName);

        CharSequence constantsDeclare = maximaCodeToDeclareConsts( Common.getInstance().getConstants(model) );
        if( constantsDeclare.length() > 0) {
            maximaRunner.run( constantsDeclare.toString() );
        }
        modelSet = true;
    }

    public iit.dsl.maxdsl.utils.MaximaRunner getMaximaRunner() { return maximaRunner; }

    /**
     * Call this function once you are done interpreting the transforms
     * of one kind.
     *
     * This function terminates the execution of the Maxima interpreter.
     */
    public void done() {
        modelSet = false;
        maximaRunner.terminate();
        maximaRunner = null;
    }
    /**
     * Interpret the Maxima code corresponding to the given transform, and
     * retrieve the result in the form of a matrix of strings.
     *
     * @param transform The Transform of interest
     * @param size The value n of the matrix size spec n x n
     *
     * @return A matrix of strings, each one being the textual output of the
     *  Maxima engine for the corresponding element of the given Transform.
     *
     * @see iit.dsl.maxdsl.utils.MaximaConversionUtils.getMatrix()
     */
    @Deprecated
    public String[][] parseTransform(Transform transform, int size) {
        if(!modelSet) {
            throw(new RuntimeException("Cannot parse any transform because no model was set"));
        }
        // The name of the transformation matrix in the Maxima code:
        String transformLiteral = null;
        switch(this.mode) {
            case PLAIN:
                transformLiteral = maxima.getTransformFunctionLiteral(transform).toString();
                break;
            case TRIGREDUCE:
                transformLiteral = "float(ratexpand(ratsimp(trigreduce("
                        + maxima.getTransformFunctionLiteral(transform).toString() +  "))))";
                break;
            case TRIGSIMP:
                transformLiteral = "trigsimp(" +
                        maxima.getTransformFunctionLiteral(transform).toString() + ")";
                break;
            case __UNKNOWN:
            default:
                throw new RuntimeException("Unknown Maxima conversion mode.");
        }

        return iit.dsl.maxdsl.utils.MaximaConversionUtils.getMatrix(
                maximaRunner, transformLiteral, size, size);
    }

    /**
     * Interpret the Maxima code corresponding to the given transform, and
     * retrieve the result in the form of a matrix of strings.
     *
     * @param transform The Transform of interest
     * @param size The value n of the matrix size spec n x n
     *
     * @return A matrix of strings, each one being the textual output of the
     *  Maxima engine for the corresponding element of the given Transform.
     *
     * @see iit.dsl.maxdsl.utils.MaximaConversionUtils.getMatrix()
     */
    public String[][] toText(Transform transform, int size)
    {
        return toText(maxima.getTransformFunctionLiteral(transform).toString(), size, size);
    }

    public String[][] toText(String matrixName, int rows, int cols)
    {
        if(!modelSet) {
            throw(new RuntimeException("Cannot parse any transform because no model was set"));
        }
        String evaluateMe = maximaExpression( matrixName, this.mode );
        return MaximaConversionUtils.getMatrix(maximaRunner, evaluateMe, rows, cols);
    }


    public ProcessedMatrixInfo process(Transform transform, int size)
    {
        return process(maxima.getTransformFunctionLiteral(transform).toString(), size, size);
    }


    public ProcessedMatrixInfo process(String mxName, int rows, int cols)
    {
        String[][] text   = toText(mxName, rows, cols);
        boolean[][]consts = MaximaConversionUtils.markConstants    (maximaRunner, mxName, rows, cols);
        boolean[][]floats = MaximaConversionUtils.markFloatLiterals(maximaRunner, mxName, rows, cols);
        return new ProcessedMatrixInfo( text, floats, consts );
    }


    public static CharSequence maximaCodeToDeclareConsts(List<Constant> constants)
    {
        StringConcatenation text = new StringConcatenation();
        for( Constant c : constants)
        {
            text.append("declare( " + Constants.toMaximaIdentifier(c) + ", constant);\n");
        }
        return text;
    }


    public static String maximaExpression(String matrixName, Mode mode)
    {
        String expr = matrixName;
        switch(mode)
        {
            case PLAIN:
                //nothing to do, the expression is just the matrix name
                break;
            case TRIGREDUCE:
                expr = "float(ratexpand(ratsimp(trigreduce(" + matrixName + "))))";
                break;
            case TRIGSIMP:
                expr = "trigsimp(" + matrixName + ")";
                break;
            case __UNKNOWN:
            default:
                throw new RuntimeException("Unknown Maxima conversion mode.");
        }
        return expr;
    }



    public static Mode parseStringMode(String mode) {
        if(mode.equals(Mode.PLAIN.asString())) {
            return Mode.PLAIN;
        }
        if(mode.equals(Mode.TRIGSIMP.asString())) {
            return Mode.TRIGSIMP;
        }
        if(mode.equals(Mode.TRIGREDUCE.asString())) {
            return Mode.TRIGREDUCE;
        }
        return Mode.__UNKNOWN;
    }


}
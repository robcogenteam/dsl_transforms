package iit.dsl.coord.generator.maxima.converter

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.generator.MaximaDSLUtils
import iit.dsl.coord.generator.ProcessedMatrixInfo
import iit.dsl.coord.generator.TransformsInfo
import iit.dsl.coord.generator.Utilities

import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.emf.ecore.EObject
import iit.dsl.coord.generator.Common

class Utils
{
    public static class TransformConversionInfo
    {
        public new(TransformsInfo.Arguments args,
                   Utilities.MatrixType type,
                   ProcessedMatrixInfo info )
        {
            basic = args
            mxtype= type
            layers= info

            for( vi : args.variables ) {
                maximaIdsToArguments.put( vi.variable.varname, vi.variable)
            }
            for( p : args.parameters) {
                maximaIdsToArguments.put( Parameters.toMaximaIdentifier(p), p )
            }
            for( c : args.constants) {
                maximaIdsToArguments.put( Constants.toMaximaIdentifier(c), c)
            }

            maximaDSLText = MaximaDSLUtils::MaximaDSLDocumentText(maximaIdsToArguments.keySet, info)
        }

        public final TransformsInfo.Arguments  basic
        public final Utilities.MatrixType mxtype
        public final ProcessedMatrixInfo layers
        public final CharSequence maximaDSLText
        public final Map<String, EObject> maximaIdsToArguments = new HashMap<String, EObject>()
    }


    def public static List<TransformConversionInfo> getConversionInfo(
                      Model model,
                      List<TransformsInfo.Arguments> basicInfo,
                      Utilities.MatrixType mxtype,
                      MaximaConverter maximaConverter)
    {
        maximaConverter.setModel(model, mxtype)

        val ret = new ArrayList<TransformConversionInfo>()

        for( info : basicInfo )
        {
            val processed = maximaConverter.process(info.transform, mxtype.size)
            ret.add( new TransformConversionInfo( info, mxtype, processed) )
        }

        maximaConverter.done()

        return ret
    }

    /**
     * Construct a map from Maxima identifiers to the properties of the
     * given transforms model.
     *
     * The map has an entry for each variable, parameter and constant in the
     * given Model. Keys are the identifiers used in the generated Maxima code.
     * Values are the Model properties themselves: variables, parameters or
     * constant.
     *
     * The returned map basically allows to recover a Model property given the
     * identifier used for the same property in Maxima code.
     */
    def public static Map<String, EObject> maximaIDsToModelArgs(Model model)
    {
        val ret = new HashMap<String, EObject>()
        val help = Common.getInstance()
        val vars = help.getVars  (model)
        val pars = help.getParams(model)
        val cons = help.getConstants(model)

        for( vi : vars ) {
            ret.put( vi.varname, vi)
        }
        for( p : pars ) {
            ret.put( Parameters.toMaximaIdentifier(p), p )
        }
        for( c : cons ) {
            ret.put( Constants.toMaximaIdentifier(c), c)
        }
        return ret
    }
}
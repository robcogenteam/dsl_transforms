package iit.dsl.coord.generator.maxima.converter

import iit.dsl.coord.coordTransDsl.Constant
import iit.dsl.coord.generator.Common
import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.maxdsl.maximaDsl.VarLiteral

class Constants
{
    /**
     * The Maxima identifier to be used for the given Constant
     */
    def public static String toMaximaIdentifier(Constant c) {
        return prefix + c.name;
    }

    /**
     * The name of the model Constant corresponding to the given Maxima identifier.
     * Null, if the identifier does not represent a constant.
     */
    def public static String maximaVarToConstantName(String mvar)
    {
        if(! mvar.startsWith(prefix)) return null
        return mvar.replaceFirst(prefix, "")
    }

    /**
     * The model Constant corresponding to the given Maxima identifier.
     * Null, if the identifier does not represent a constant.
     */
    def public static Constant maximaVarToConstant(String mvar, Model model)
    {
        val constName = maximaVarToConstantName(mvar);
        if(constName === null) return null

        val cnt = common.getConstantByName(model, constName)
        if( cnt === null ) {
                throw(new RuntimeException("Could not find a constant called " +
                        constName + " in model " + model.getName()));
        }
        return cnt
    }

    /**
     * The model Constant corresponding to the given Maxima identifier.
     * Null, if the identifier does not represent a constant.
     */
    def public static Constant maximaVarToConstant(VarLiteral mvar, Model model)
    {
        return maximaVarToConstant(mvar.value.name, model)
    }

    private static extension Common common = Common::getInstance()
    private static String prefix = "_k__"
}
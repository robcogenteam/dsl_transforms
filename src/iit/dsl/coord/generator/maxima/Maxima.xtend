package iit.dsl.coord.generator.maxima

import iit.dsl.coord.coordTransDsl.Rx
import iit.dsl.coord.coordTransDsl.Ry
import iit.dsl.coord.coordTransDsl.Rz
import iit.dsl.coord.coordTransDsl.Tx
import iit.dsl.coord.coordTransDsl.Ty
import iit.dsl.coord.coordTransDsl.Tz
import iit.dsl.coord.coordTransDsl.Rotation
import iit.dsl.coord.coordTransDsl.Translation
import iit.dsl.coord.coordTransDsl.FloatLiteral
import iit.dsl.coord.coordTransDsl.PlainExpr
import iit.dsl.coord.coordTransDsl.MultExpr
import iit.dsl.coord.coordTransDsl.DivExpr
import iit.dsl.coord.coordTransDsl.Expr
import iit.dsl.coord.coordTransDsl.PILiteral
import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Transform
import iit.dsl.coord.coordTransDsl.Variable
import iit.dsl.coord.coordTransDsl.AbsMatrix
import iit.dsl.coord.coordTransDsl.impl.CoordTransDslFactoryImpl
import iit.dsl.coord.coordTransDsl.Parameter
import iit.dsl.coord.coordTransDsl.ParameterLiteral
import iit.dsl.coord.coordTransDsl.Constant

import iit.dsl.coord.generator.Common
import iit.dsl.coord.generator.Utilities
import iit.dsl.coord.generator.ParameterFQN
import iit.dsl.coord.generator.maxima.converter.Constants

import java.util.List
import org.eclipse.xtend2.lib.StringConcatenation
import iit.dsl.coord.generator.maxima.converter.Parameters

class Maxima
{
    @Deprecated
    def public static String parameterToMaximaVarName(ParameterLiteral param)
    '''«paramNamePrefix»«Common::getGroup(param).name»«paramNameSeparator»«param.name»'''

    @Deprecated
    def public static ParameterFQN maximaVarNameToParamName(String varname) {
        if(! varname.startsWith(paramNamePrefix)) return null
        val tmp = varname.split(paramNameSeparator)
        if(tmp.size != 2) {
            throw(new RuntimeException(
            "Could not guess the parameter group and parameter name from the variable name " + varname))
        }
        val ret = new ParameterFQN
        ret.groupName = tmp.head.replaceFirst(paramNamePrefix, "")
        ret.paramName = tmp.last

        return ret
    }

    private static String paramNamePrefix    = "__p__";
    private static String paramNameSeparator = "___";

    /**
     * The name of the Maxima function that will be used for the given transform.
     */
    def public static transformFunctionLiteral(Transform t) {
        return t.name + "(" + argsListText(t) + ")"
    }


    private static extension Common common = Common::getInstance()


    /**
     * The main generation function for Maxima code.
     * \param model the coordinate transforms model containing the abstract
     *        transforms to be turned into Maxima code
     * \param maxima an instance in the Maxima class hierarchy that determines
     *        whether the generated code is for homogeneous transforms or
     *        spatial vectors transforms (velocity or force)
     */
    def public static  CharSequence sourceCode(Model model, Maxima maxima) {
        var Utilities$MatrixConvention modelConvention;
        if(model.convention.equals("left")) {
            modelConvention = Utilities$MatrixConvention::TRANSFORMED_FRAME_ON_THE_LEFT
        } else {
            modelConvention = Utilities$MatrixConvention::TRANSFORMED_FRAME_ON_THE_RIGHT
        }

        var StringConcatenation text = new StringConcatenation();
        for(Transform t : model.transforms) {
            var List<AbsMatrix> matrices
            matrices = t.matrices

            text.append('''«maxima.getTransformFunctionLiteral(t)» := ''')
            if( ! matrices.empty) {
                text.append('''«maxima.getCode(matrices.get(0), modelConvention)»''');
                for(AbsMatrix mx : matrices.drop(1)) {
                    text.append(''' . «maxima.getCode(mx, modelConvention)»''');
                }
            } else {
                var Rx identity = CoordTransDslFactoryImpl::init().createRx();
                var FloatLiteral zero = CoordTransDslFactoryImpl::init().createFloatLiteral();
                zero.setValue(0)
                identity.setArg(zero)
                text.append(maxima.getCode(identity));
            }
            text.append(";\n");
        }
        return text
    }

    def static String transformsFileName(Model model) {
        return model.name + "_transforms"
    }
    def static String transforms6DFileName(Model model) {
        return model.name + "_transforms6D"
    }
    def static String transforms6DForceFileName(Model model) {
        return model.name + "_transforms6DForce"
    }


    /**
     * Tells which convention is adopted by the matrices available to this generator
     * TODO read this from configuration !!!
     */
    def Utilities$MatrixConvention getConvention() {
        return Utilities$MatrixConvention::TRANSFORMED_FRAME_ON_THE_RIGHT;
    }

    def dispatch CharSequence str(PlainExpr expr) '''«str(expr.identifier)»'''
    def dispatch CharSequence str(MultExpr expr) {
        if(Utilities::isInteger(expr.mult)) {
            '''ratsimp(«Utilities::asInteger(expr.mult)»*«str(expr.identifier)»)'''
        } else {
            '''ratsimp(«expr.mult»*«str(expr.identifier)»)'''
        }
    }
    def dispatch CharSequence str(DivExpr expr)
    {
        var String div = expr.div.toString()
        if(Utilities::isInteger(expr.div)) {
            div = Utilities::asInteger(expr.div).toString()
        }
        '''(«str(expr.identifier)» / «div»)'''
    }
    def dispatch CharSequence str(Variable  va)  '''«IF va.minus»-«ENDIF»«va.varname»'''
    def dispatch CharSequence str(Parameter pa)  '''«IF pa.minus»-«ENDIF»«Parameters.toMaximaIdentifier(pa.param)»'''
    def dispatch CharSequence str(PILiteral pi)  '''«IF pi.minus»-«ENDIF»%pi'''

    def dispatch CharSequence getCode(Expr arg)          '''«str(arg)»'''
    def dispatch CharSequence getCode(FloatLiteral arg)  '''«arg.value»'''
    def dispatch CharSequence getCode(Constant ct)  '''«IF ct.minus»-«ENDIF»«Constants.toMaximaIdentifier(ct)»'''

    def dispatch CharSequence getCode(Rotation r, Utilities$MatrixConvention convention) {
        if(getConvention() == convention) {
            '''«getCode(r)»'''
        } else {
            '''transpose(«getCode(r)»)'''
       }
    }
    def dispatch CharSequence getCode(Translation t, Utilities$MatrixConvention convention) {
        if(getConvention() == convention) {
            '''«getCode(t)»'''
        } else {
            '''invert(«getCode(t)»)'''
       }
    }
	def dispatch CharSequence getCode(Rx rot)
	'''«rot.name»(«rot.arg.code»)'''
	def dispatch CharSequence getCode(Ry rot)
    '''«rot.name»(«rot.arg.code»)'''
    def dispatch CharSequence getCode(Rz rot)
    '''«rot.name»(«rot.arg.code»)'''
    def dispatch CharSequence getCode(Tx tr)
    '''«tr.name»([«tr.arg.code»,0,0])'''
    def dispatch CharSequence getCode(Ty tr)
    '''«tr.name»([0,«tr.arg.code»,0])'''
    def dispatch CharSequence getCode(Tz tr)
    '''«tr.name»([0,0,«tr.arg.code»])'''

    def dispatch getName(Rx rot)'''Rhx'''
    def dispatch getName(Ry rot)'''Rhy'''
    def dispatch getName(Rz rot)'''Rhz'''
    def dispatch getName(Tx rot)'''Th'''
    def dispatch getName(Ty rot)'''Th'''
    def dispatch getName(Tz rot)'''Th'''

    def getTransformFunctionLiteral(Transform t) {
        return transformFunctionLiteral(t)
    }
}

package iit.dsl.coord.generator.maxima

import iit.dsl.coord.coordTransDsl.Rx
import iit.dsl.coord.coordTransDsl.Ry
import iit.dsl.coord.coordTransDsl.Rz
import iit.dsl.coord.coordTransDsl.Tx
import iit.dsl.coord.coordTransDsl.Ty
import iit.dsl.coord.coordTransDsl.Tz
import iit.dsl.coord.generator.Utilities

class Maxima6D extends Maxima {
	override dispatch getName(Rx rot)'''Rx6d'''
	override dispatch getName(Ry rot)'''Ry6d'''
	override dispatch getName(Rz rot)'''Rz6d'''
	override dispatch getName(Tx rot)'''T6d'''
	override dispatch getName(Ty rot)'''T6d'''
	override dispatch getName(Tz rot)'''T6d'''

	override Utilities$MatrixConvention getConvention() {
        return Utilities$MatrixConvention::TRANSFORMED_FRAME_ON_THE_RIGHT;
    }
}
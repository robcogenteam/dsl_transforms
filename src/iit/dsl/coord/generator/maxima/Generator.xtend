package iit.dsl.coord.generator.maxima

import org.eclipse.xtext.generator.IGenerator
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess

import iit.dsl.coord.coordTransDsl.Model


class Generator implements IGenerator {

    override doGenerate(Resource resource, IFileSystemAccess fsa) {
        val model = resource.contents.head as Model;
        generateAll(model, fsa)
    }

    def public void generateAll(Model model, IFileSystemAccess fsa) {
        fsa.generateFile(Maxima::transformsFileName(model),   Maxima::sourceCode(model, new Maxima()))
        fsa.generateFile(Maxima::transforms6DFileName(model),      Maxima::sourceCode(model, new Maxima6D()))
        fsa.generateFile(Maxima::transforms6DForceFileName(model), Maxima::sourceCode(model, new Maxima6DForce()))
    }

}
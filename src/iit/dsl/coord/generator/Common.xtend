package iit.dsl.coord.generator

import java.util.ArrayList
import java.util.List
import java.util.Set
import java.util.HashSet
import java.util.LinkedHashSet

import iit.dsl.coord.coordTransDsl.AbsMatrix
import iit.dsl.coord.coordTransDsl.CoordTransDslPackage
import iit.dsl.coord.coordTransDsl.Expr
import iit.dsl.coord.coordTransDsl.FloatLiteral
import iit.dsl.coord.coordTransDsl.Identifier
import iit.dsl.coord.coordTransDsl.Transform

import iit.dsl.coord.coordTransDsl.ArgSpec
import iit.dsl.coord.coordTransDsl.Rotation
import iit.dsl.coord.coordTransDsl.Translation
import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Frame

import iit.dsl.coord.coordTransDsl.Variable
import iit.dsl.coord.coordTransDsl.ParameterLiteral
import iit.dsl.coord.coordTransDsl.Parameter
import iit.dsl.coord.coordTransDsl.ParametersDeclaration
import iit.dsl.coord.coordTransDsl.Constant
import org.eclipse.emf.ecore.EObject

class Common {
	def name(Transform T) {
	    if(T.userName != null) return T.userName;
	    return T.leftFrame.name + "_X_" + T.rightFrame.name
	}

    /**
     * Returns the list of constants of the given Transform.
     */
    def List<Constant> getConstants(Transform T)
    {
        val ret = new ArrayList<Constant>();
        val names = new HashSet<String>(); // used to check whether a variable has been already added
        for(AbsMatrix matrix : T.matrices) {
            if(matrix.arg instanceof Constant) {
                val c = matrix.arg as Constant
                if( ! names.contains(c.name)) {
                    ret.add(c)
                    names.add(c.name)
                }
            }
        }
        return ret;
    }

    def List<Constant> getConstants(Model m)
    {
        val names = new HashSet<String>() // used to check whether a variable has been already added
        val consts= new ArrayList<Constant>()
        for(t : m.transforms) {
            val list = getConstants(t)
            for(c : list) {
                if( ! names.contains(c.name) ) {
                    consts.add(c)
                    names.add(c.name)
                }
            }
        }
        return consts
    }

    def Constant getConstantByName(Transform t, String cname) {
        val all = getConstants(t)
        for( c : all ) {
            if( c.name.equals(cname) ) {
                return c
            }
        }
        return null
    }

    def Constant getConstantByName(Model model, String cname) {
        for( c : getConstants(model) ) {
            if( c.name.equals(cname) ) {
                return c
            }
        }
        return null
    }

    /**
     * Returns the list of variables of the given Transform.
     */
    def List<Variable> getVars(Transform T) {
        val List<Variable> ret = new ArrayList<Variable>();
        val varNames = new HashSet<String>(); // used to check whether a variable has been already added
        for(AbsMatrix matrix : T.matrices) {
            if(matrix.arg instanceof Expr) {
                val Expr argument = matrix.arg as Expr
                if(argument.identifier.eClass().getClassifierID() == CoordTransDslPackage::VARIABLE) {
                    val variable = argument.identifier as Variable
                    if( ! varNames.contains(variable.varname)) {
                        ret.add(variable)
                        varNames.add(variable.varname)
                    }
                }
            }
        }
        return ret;
    }

    /**
     * The list of all the variables referenced in
     * the given Model, in the order they are found in the model.
     * @param m the Model (i.e. a list of transforms) whose variables have to be
     *        returned
     * @return a list of all the unique variables referenced by all the transforms
     *         in the given Model, in the same order as they appear in the
     *         original document.
     */
    def List<Variable> getVars(Model m) {
        val varNames = new HashSet<String>() // used to check whether a variable has been already added
        val allVars  = new ArrayList<Variable>()
        for(t : m.transforms) {
            val list = getVars(t)
            for(v : list) {
                if( ! varNames.contains(v.varname) ) {
                    allVars.add(v)
                    varNames.add(v.varname)
                }
            }
        }
        return allVars
    }


    def argsListText(Transform t) {
        argsListText(t.vars)
    }
    def argsListText(List<Variable> args) '''
        «FOR arg : args SEPARATOR ", "»«arg.varname»«ENDFOR»'''


    def dispatch String getVariableName(Expr expr) {
        val Identifier identifier = expr.identifier
        if(identifier.eClass().getClassifierID() == CoordTransDslPackage::VARIABLE) {
            return (identifier as Variable).varname
        }
        return null
    }
    def dispatch String getVariableName(FloatLiteral flo) {
        return null
    }

    def dispatch boolean contains(ArgSpec arg, Identifier identifier) {
        return false //default
    }
    def dispatch boolean contains(Expr arg, Identifier identifier) {
        return arg.identifier.equals(identifier)
    }
    /**
     * Tells whether the specified variable is the argument of a rotation in the
     * specified transform
     */
    def boolean specifiesRotation(Variable variable, Transform t) {
        for(AbsMatrix matrix : t.matrices) {
            if(matrix.arg.contains(variable)) {
                return(matrix instanceof Rotation)
            }
        }
        throw(new RuntimeException("This transform is not a function of the passed variable"))
    }
    /**
     * Tells whether the specified variable is the argument of a translation in the
     * specified transform
     */
    def boolean specifiesTranslation(Variable variable, Transform t) {
        for(AbsMatrix matrix : t.matrices) {
            if(matrix.arg.contains(variable)) {
                return(matrix instanceof Translation)
            }
        }
        throw(new RuntimeException("This transform is not a function of the passed variable"))
    }



    def getParams(Transform T) {
        val List<ParameterLiteral> ret = new ArrayList<ParameterLiteral>();
        val names = new HashSet<String>(); // used to check whether a parameter has been already added
        for(AbsMatrix matrix : T.matrices) {
            if(matrix.arg instanceof Expr) {
                val Expr argument = matrix.arg as Expr
                if(argument.identifier.eClass().getClassifierID() == CoordTransDslPackage::PARAMETER) {
                    val param = (argument.identifier as Parameter).param
                    val key = new ParameterFQN(param).toString()
                    if( ! names.contains(key)) {
                        ret.add(param)
                        names.add(key)
                    }
                }
            }
        }
        return ret;
    }

    def getParamsGroups(Transform T) {
        val params = getParams(T)
        val ret   = new ArrayList<ParametersDeclaration>();
        val names = new HashSet<String>(); // used to check whether a parameter has been already added
        var ParametersDeclaration group = null
        for (p : params) {
            group = getGroup(p)
            if( ! names.contains(group.name)) {
                ret.add(group)
                names.add(group.name)
            }
        }
        return ret
    }

    /**
     * The list of all the parameters referenced in
     * the given Model, in the order they are found in the model.
     * @param m the Model (i.e. a list of transforms) whose parameters have to
     *          be returned
     * @return a list of all the unique parameters referenced by all the
     *         transforms in the given Model, in the same order as they appear
     *         in the original document.
     */
    def List<ParameterLiteral> getParams(Model m) {
        val allParams = new LinkedHashSet<ParameterLiteral>()
        for(t : m.transforms) {
            val list = getParams(t)
            for(p : list) {
                allParams.add(p)
            }
        }
        return allParams.toList
    }

    def paramsListText(Transform t) {
        paramsListText(t.params)
    }

    def paramsListText(List<ParameterLiteral> args) '''
        «FOR arg : args SEPARATOR ", "»«arg.name»«ENDFOR»'''


    def static getGroup(ParameterLiteral param) {
        return param.eContainer as ParametersDeclaration
    }

    def static getParametersGroupFromName(Model model, String groupName) {
        for(g : model.paramGroups) {
            if(g.name.equals(groupName)) return g
        }
        return null
    }

    def static getParameterFromName(Model model, ParameterFQN fqn) {
        var group = getParametersGroupFromName(model, fqn.groupName)
        if(group == null) return null
        for(p : group.params) {
            if(p.name.equals(fqn.paramName)) return p
        }
        return null
    }

    def static boolean dependsOnParameters(Model model) {
        if(model.paramGroups == null) return false

        for( gr : model.paramGroups ) {
            if(gr.params.size > 0) return true
        }
        return false
    }

    /**
     * Tells whether the specified Parameters is the argument of a rotation in the
     * specified transform
     */
    def Set<ParameterLiteral> rotationParameters(Model model)
    {
        val ret = new HashSet<ParameterLiteral>()
        val helper = CoordTransDslPackage.eINSTANCE
        for(tf : model.transforms) {
            for(mx : tf.matrices) {
                if(helper.rotation.isSuperTypeOf( mx.eClass ) ) {
                    if(helper.expr.isSuperTypeOf( mx.arg.eClass ) ) {
                        val id = (mx.arg as Expr).identifier
                        if(helper.parameter.isSuperTypeOf(id.eClass)) {
                            ret.add( (id as Parameter).param )
                        }
                    }
                }
            }
        }
        return ret
    }

    def Transform getTransform(Model model, Frame left, Frame right) {
        // the check must be on the name, because the user cannot easily build an
        //  instance of Frame which is actually equal to one of the model (because of
        //  the fancy internal 'e<something>' properties of the objects
        return getTransform(model, left.name, right.name)
    }
    def Transform getTransform(Model model, String leftFrameName, String rightFrameName) {
        for (t : model.transforms) {
            if(t.leftFrame.name.equals(leftFrameName) && t.rightFrame.name.equals(rightFrameName)) {
                return t;
            }
        }
        return null;
    }

    def boolean areEqual(Transform t1, Transform t2) {
        return( t1.leftFrame.name.equals(t2.leftFrame.name)  &&  t1.rightFrame.name.equals(t2.rightFrame.name) );
    }

    /**
     * Tells whether two transforms are one the inverse of the other.
     * \return true if the two parameters are in the form A_X_B and B_X_A, for
     *         any reference frame A and B, so that their composition is the
     *         identity. False otherwise.
     */
    def boolean areInverse(Transform t1, Transform t2) {
        return( t1.leftFrame.name.equals(t2.rightFrame.name)  &&  t1.rightFrame.name.equals(t2.leftFrame.name) );
    }

    def List<Transform> noDuplicatesTransformsList(Model model) {
        val List<Transform> ret = new ArrayList<Transform>();
        var boolean alreadyThere = false;
        for(newt : model.transforms) {
            val iter = ret.iterator()
            alreadyThere = false

            while( !alreadyThere  &&  iter.hasNext() ) {
                if( areEqual(newt, iter.next()) ) {
                    alreadyThere = true
                }
            }
            if( !alreadyThere ) {
                ret.add(newt)
            }
        }
        return ret
    }


    def public boolean isVariable(EObject obj) {
        return CoordTransDslPackage.Literals.VARIABLE.isInstance(obj)
    }
    def public boolean isParameter(EObject obj) {
        return CoordTransDslPackage.Literals.PARAMETER_LITERAL.isInstance(obj)
    }
    def public boolean isConstant(EObject obj) {
        return CoordTransDslPackage.Literals.CONSTANT.isInstance(obj)
    }

    def public static getInstance() { return instance }
    private static Common instance = new Common()
}

class ParameterFQN {
    public String groupName;
    public String paramName;

	new () {}

	new(String group, String name) {
	    groupName = group
	    paramName = name
	}

    new(ParameterLiteral p) {
        groupName = Common::getGroup(p).name
        paramName = p.name
    }

    override public toString() {
        return groupName + "." + paramName;
    }
}

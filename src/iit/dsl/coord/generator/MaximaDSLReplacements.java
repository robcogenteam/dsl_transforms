package iit.dsl.coord.generator;

import java.util.Map;
import org.eclipse.emf.ecore.EObject;

import iit.dsl.coord.coordTransDsl.Constant;
import iit.dsl.coord.coordTransDsl.Model;
import iit.dsl.coord.coordTransDsl.ParameterLiteral;
import iit.dsl.coord.coordTransDsl.Variable;

import iit.dsl.coord.generator.Common;
import iit.dsl.maxdsl.maximaDsl.Cosine;
import iit.dsl.maxdsl.maximaDsl.MaximaDslPackage;
import iit.dsl.maxdsl.maximaDsl.Sine;
import iit.dsl.maxdsl.maximaDsl.VarLiteral;


/**
 * Default implementation of the {@link iit.dsl.maxdsl.generator.IIdentifiersReplacement}
 * interface required by helper generators of the MaximaDSL.
 *
 * This class depends in turn on three other
 * configuration interfaces, {@link IVariablesAccess}, {@link IParametersAccess}
 * and {@link IConstantsAccess}, and it is therefore generic and reusable.
 *
 * This class basically takes care of resolving the MaximaDSL objects into the
 * corresponding objects of a transforms model (TransformsDSL, this package),
 * this being Variable, Parameter or Constant. The actual conversion into
 * expressions in the target language is delegated to the three configuration
 * interfaces, as said above.
 *
 * @author Marco Frigerio
 */
public class MaximaDSLReplacements implements iit.dsl.maxdsl.generator.IIdentifiersReplacement
{
    public MaximaDSLReplacements(Model m, IVariablesAccess vars, IParametersAccess pars, IConstantsAccess consts)
    {
        model = m;
        varsAccess = vars;
        parsAccess = pars;
        constAccess= consts;
        varNamesMap = iit.dsl.coord.generator.maxima.converter.Utils.maximaIDsToModelArgs(model);
    }

    public IVariablesAccess getVariablesAccess() { return varsAccess; }

    @Override
    public String valueExpression(VarLiteral var)
    {
        String varname = var.getValue().getName();
        EObject obj = varNamesMap.get( varname );

        if( common.isVariable(obj) ) {
            return varsAccess.valueExpression(model, (Variable)obj);
        }

        if( common.isParameter(obj) ) {
            return parsAccess.valueExpression(model, (ParameterLiteral)obj);
        }

        if( common.isConstant(obj) ) {
            return constAccess.valueExpression(model, (Constant)obj);
        }

        return throwVarNotFound(varname);
    }

    @Override
    public String valueExpression(Sine sine)
    {
        final String fstr = "sine";
        VarLiteral sinearg = asVarLiteral( sine.getArg() );

        return funcValueExpression(sinearg, fstr);
    }


    @Override
    public String valueExpression(Cosine cosine)
    {
        final String fstr = "cosine";
        VarLiteral cosinearg = asVarLiteral( cosine.getArg() );

        return funcValueExpression(cosinearg, fstr);
    }



    private String funcValueExpression(VarLiteral farg, String fstr)
    {
        String varname = farg.getValue().getName();
        EObject obj = varNamesMap.get( varname );

        if( common.isVariable(obj) ) {
            return varsAccess.valueExpression(model, (Variable)obj, fstr);
        }

        if( common.isParameter(obj) ) {
            return parsAccess.valueExpression(model, (ParameterLiteral)obj, fstr);
        }

        if( common.isConstant(obj) ) {
            return constAccess.valueExpression(model, (Constant)obj, fstr);
        }

        return throwVarNotFound(varname);
    }

    private VarLiteral asVarLiteral(iit.dsl.maxdsl.maximaDsl.Expression arg)
    {
        if( ! MaximaDslPackage.Literals.VAR_LITERAL.isInstance( arg ) ) {
            throw new RuntimeException("Only functions of plain variables can be converted");
        }

        return (VarLiteral)arg;
    }

    private String throwVarNotFound(String varname) throws RuntimeException {
        throw new RuntimeException("Could not trace MaximaDSL variable " + varname + " back to a TransformsDSL variable/parameter/constant");
    }


    private Model model = null;
    private IVariablesAccess varsAccess = null;
    private IParametersAccess parsAccess= null;
    private IConstantsAccess constAccess= null;
    private Map<String, EObject> varNamesMap = null;
    private Common common = Common.getInstance();
}

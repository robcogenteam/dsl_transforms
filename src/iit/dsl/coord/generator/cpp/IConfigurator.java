package iit.dsl.coord.generator.cpp;

import java.util.List;

import iit.dsl.coord.coordTransDsl.Model;
import iit.dsl.coord.generator.Utilities;


/**
 * Configuration information required by the C++ code generator of this package.
 *
 * The user can tune the generated C++ code by providing specific
 * implementations of this interface.
 * @author Marco Frigerio
 *
 */
public interface IConfigurator
{

    /**
     * The name of the header file to be generated.
     *
     * The name should not include the extension '.h', which will be added by
     * default.
     */
    public String headerFileName(Model model);
    //public String sourceFileName(Model model);

    public String paramsHeaderFileName(Model model);

    /**
     * The namespaces that should enclose all the generated code.
     * @param model
     * @return a list of namespace names, ordered from the outermost to the
     *          innermost
     */
    public List<String> enclosingNamespaces(Model model);

    /**
     * The text containing additional \c include directives to be added to the
     * generated code.
     * Such directives may contain for example the header files defining the
     * type to be used for the transforms
     * (see matrixType(Model, Utilities.MatrixType)).
     */
    public CharSequence includeDirectives(Model model);

    /**
     * The name of the C++ class to be used for the given transform type.
     *
     * This class will be nested within the enclosing namespaces defined
     * by another function of this interface.
     * @param matrixtype the type of the transforms whose class has to be
     *         returned.
     * @see enclosingNamespaces, Utilities.MatrixType
     */
    public String className(Utilities.MatrixType matrixtype);

    /**
     * A valid C++ type to be used for the implementation of matrices.
     * E.g. 'Eigen::Matrix<double,4,4>'
     * @param transformtype the type of the transformation matrix whose C++
     *         type has to be returned
     * @return a String that can be used in a C++ source as a matrix type
     * @see Utilities.MatrixType
     */
    public String matrixBaseType(Model model, Utilities.MatrixType transformtype);

    /**
     * The type name for a typedef of the matrixType.
     */
    public String localTypeName(Utilities.MatrixType matrixtype);

    /**
     * The maxima converter configurator required for the generation of C++ code.
     */
    public iit.dsl.coord.generator.maxima.converter.IConfigurator
    getMaximaConverterConfigurator();

    public IVariablesAccess  getVariablesValueExprGenerator(Model model);
    public IConstantsAccess  getConstantsValueExprGenerator(Model model);

    public String scalarType();
    public String sinFunctionName();
    public String cosFunctionName();
}

package iit.dsl.coord.generator.cpp

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.IFileSystemAccess
import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.generator.cpp.IConfigurator
import iit.dsl.coord.generator.cpp.Parameters


class Generator implements IGenerator
{
    private Parameters paramsGetterGen = null
    private IConfigurator configurator = null
    private ContainerClass mainGen = null;
	
	new() {
	    this(new DefaultConfigurator)
    }

    new(IConfigurator conf) {
        configurator = conf
        paramsGetterGen = new Parameters()
        mainGen = new ContainerClass(configurator)
    }

	override void doGenerate(Resource resource, IFileSystemAccess fsa)
	{
        val model = resource.contents.head as Model;
        mainGen.setModel(model)
        val dir = model.name + "/"
        val mainfile = configurator.headerFileName(model)
        fsa.generateFile(dir + mainfile + ".h"  , mainGen.headerFile() )
        fsa.generateFile(dir + mainfile + ".cpp", mainGen.sourceFile() )
        fsa.generateFile(dir + configurator.paramsHeaderFileName(model) + ".h",
            paramsGetterGen.headerFileContent(model, #[""], configurator.enclosingNamespaces(model), configurator.scalarType)
        )
    }
}
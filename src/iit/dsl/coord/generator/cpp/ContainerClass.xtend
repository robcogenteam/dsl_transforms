package iit.dsl.coord.generator.cpp

import java.util.List
import java.util.Set

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Transform
import iit.dsl.coord.coordTransDsl.ParametersDeclaration
import iit.dsl.coord.coordTransDsl.ParameterLiteral
import iit.dsl.coord.generator.Utilities
import iit.dsl.coord.generator.Common
import iit.dsl.coord.generator.TransformsInfo
import iit.dsl.coord.generator.cpp.ContainerClass.ParametersStructMeta.ParamsGroup


/**
 * The main generator of the C++ source and header file with the
 * implementation of coordinate transforms.
 */
class ContainerClass
{
    public static String varsvector_t = "state_t"
    /**
     *
     */
    def public static methodName_updateParams(Model model) {
        return "updateParams"
    }
    /**
     * The name of the member variable that represents the given transform
     */
    def public static memberName(Transform t) {
        t.name //use the same name convention implemented in Common
    }


    def public static nsQualifier(IConfigurator configurator, Model model)
    '''«FOR ns : configurator.enclosingNamespaces(model) SEPARATOR "::"»«ns»«ENDFOR»'''



    public new() {
        this(new DefaultConfigurator)
    }

    public new(IConfigurator config)
    {
        configurator   = config
    }

    def public void setModel(Model m)
    {
        model = m
        transformsData = TransformsInfo.generalInfo( model )
        classesGen     = new LocalClasses(model, transformsData,
                                    configurator, use_cppusing)

        val paramsStructConfig = new ParametersStructMeta.IConfigurator() {
            override scalarType()      { return configurator.scalarType }
            override sinFunctionName() { return configurator.sinFunctionName }
            override cosFunctionName() { return configurator.cosFunctionName }
        }
        paramsStruct = new ParametersStructMeta(model, paramsStructConfig)
    }

    /**
     * C++ code for the header file.
     */
    def headerFile()
    {
        if(model===null) { throw new RuntimeException("No model was set - cannot generate code") }
        return
        '''
        #ifndef «model.name.toUpperCase()»_«configurator.headerFileName(model).toUpperCase()»_H_
        #define «model.name.toUpperCase()»_«configurator.headerFileName(model).toUpperCase()»_H_

        «configurator.includeDirectives(model)»
        #include "«configurator.paramsHeaderFileName(model)».h"

        «FOR ns : configurator.enclosingNamespaces(model)»
            namespace «ns» {
        «ENDFOR»

        «paramsStruct.definition»

        // The type of the "vector" with the status of the variables
        typedef «configurator.getVariablesValueExprGenerator(model).variablesStatusVector_type(model)» «varsvector_t»;

        template<class M>
        using «localType(mxtype_motion)» = «superType(mxtype_motion)»;

        template<class M>
        using «localType(mxtype_force)» = «superType(mxtype_force)»;

        template<class M>
        using «localType(mxtype_homog)» = «superType(mxtype_homog)»;

        /**
         * The class for the 6-by-6 coordinates transformation matrices for
         * spatial motion vectors.
         */
        «classDefinition(Utilities$MatrixType::_6D)»

        /**
         * The class for the 6-by-6 coordinates transformation matrices for
         * spatial force vectors.
         */
        «classDefinition(Utilities$MatrixType::_6D_FORCE)»

        /**
         * The class with the homogeneous (4x4) coordinates transformation
         * matrices.
         */
        «classDefinition(Utilities$MatrixType::HOMOGENEOUS)»

        «FOR ns : configurator.enclosingNamespaces(model)»
            }
        «ENDFOR»

        #endif
        '''
    }

    /**
     * C++ code for the source file with definitions
     */
    def sourceFile()
    {
        if(model===null) { throw new RuntimeException("No model was set - cannot generate code") }
        return
        '''
        #include "«configurator.headerFileName(model)».h"

        «IF use_cppusing»
            using namespace «nsQualifier(configurator,model)»;
        «ENDIF»

        // Constructors

        «constructorImpl (mxtype_motion)»
        «updateParamsImpl(mxtype_motion)»

        «constructorImpl (mxtype_force)»
        «updateParamsImpl(mxtype_force)»

        «constructorImpl (mxtype_homog)»
        «updateParamsImpl(mxtype_homog)»

        «classesGen.localClassesDefinitions(mxtype_motion)»

        «classesGen.localClassesDefinitions(mxtype_force)»

        «classesGen.localClassesDefinitions(mxtype_homog)»

        '''
    }

    def private superType(Utilities$MatrixType mxtype)
        '''«configurator.matrixBaseType(model, mxtype)»<«varsvector_t», M>'''

    def private member_paramsContainer() { return "params" }

    def private classQualifier(Utilities$MatrixType mxtype)
    {
        val qual = if(use_cppusing) "" else nsQualifier(configurator,model) + "::"
        return qual + configurator.className(mxtype)
    }

    def private classDefinition(Utilities$MatrixType mxtype)
    '''
        «val CLASS   = configurator.className(mxtype)»
        class «CLASS»
        {
        public:
            class Dummy {};
            typedef «localType(mxtype)»<Dummy>::MatrixType MatrixType;

            «classesGen.localClassesDeclaration(mxtype)»
        public:
            «CLASS»();
            void «methodName_updateParams(model)»(«FOR gr:paramsStruct.groups SEPARATOR ', '»const «ParametersStructMeta.ParamsGroup.type(gr)»&«ENDFOR»);

            «FOR tData : transformsData»
                «localClassName(tData.transform)» «memberName(tData.transform)»;
            «ENDFOR»

        protected:
            Parameters «member_paramsContainer»;

        }; //class '«configurator.className(mxtype)»'
    '''

    def private constructorImpl(Utilities$MatrixType mxtype)
    '''
        «classQualifier(mxtype)»::«configurator.className(mxtype)»()
            «FOR tData : transformsData BEFORE ' : ' SEPARATOR ','»
                «tData.transform.name»(«IF tData.isParametric»«member_paramsContainer»«ENDIF»)
            «ENDFOR»
        {}
    '''

    def private updateParamsImpl(Utilities$MatrixType mxtype)
    '''
        «val groups = paramsStruct.groups»
        «val root = member_paramsContainer»
        void «classQualifier(mxtype)»::«methodName_updateParams(model)»(«FOR gr:groups SEPARATOR ', '»const «ParametersStructMeta.ParamsGroup.type(gr)»& v_«gr.name»«ENDFOR»)
        {
            «FOR gr : groups»
                «root».«ParametersStructMeta.ParamsGroup.field(gr)» = v_«gr.name»;
            «ENDFOR»
            «val field = ParametersStructMeta.AngleFunctions.field»
            «val update= ParametersStructMeta.AngleFunctions.updateFuncName»
            «root».«field».«update»(«FOR p:paramsStruct.angles SEPARATOR','»«root».«ParametersStructMeta.accessor(p)»«ENDFOR»);
        }
    '''



    private IConfigurator   configurator    = null
    private boolean use_cppusing = true
    private extension LocalClasses classesGen = null
    private Model model = null
    private List<TransformsInfo$Arguments> transformsData = null
    private ParametersStructMeta paramsStruct = null

    private Utilities$MatrixType mxtype_motion = Utilities$MatrixType::_6D
    private Utilities$MatrixType mxtype_force  = Utilities$MatrixType::_6D_FORCE
    private Utilities$MatrixType mxtype_homog  = Utilities$MatrixType::HOMOGENEOUS

    private static extension Common common = Common.getInstance()


    public static class ParametersStructMeta
    {
        public static interface IConfigurator {
            def public String scalarType()
            def public String sinFunctionName()
            def public String cosFunctionName()
        }

        public new(Model model, IConfigurator config)
        {
            groups = model.paramGroups
            angles = model.rotationParameters
            cfg    = config
        }

        def public groups() { return this.groups }
        def public angles() { return this.angles }
        def public typeName() { return Root.type }
        def public definition()
        '''
            struct «Root.type»
            {
                struct «AngleFunctions.type» {
                    «FOR par : angles»
                        «cfg.scalarType» «AngleFunctions.sinFieldName(par)»;
                        «cfg.scalarType» «AngleFunctions.cosFieldName(par)»;
                    «ENDFOR»
                    «AngleFunctions.type»(«angleParamsAsFormalArgs») {
                        «AngleFunctions.updateFuncName»(«angleParamsAsArgs»);
                    }

                    void «AngleFunctions.updateFuncName»(«angleParamsAsFormalArgs»)
                    {
                        «FOR par : angles»
                            «AngleFunctions.sinFieldName(par)» = «cfg.sinFunctionName»(«par.name»);
                            «AngleFunctions.cosFieldName(par)» = «cfg.cosFunctionName»(«par.name»);
                        «ENDFOR»
                    }
                };

                «FOR gr : groups»
                    «ParamsGroup.type(gr)» «ParamsGroup.field(gr)»;
                «ENDFOR»
                «AngleFunctions.type» «AngleFunctions.field» = «AngleFunctions.type»(«FOR p:angles SEPARATOR','»«accessor(p)»«ENDFOR»);
            };
        '''

        def public angleParamsAsArgs()
        '''«FOR p:angles SEPARATOR','»«p.name»«ENDFOR»'''


        def public angleParamsAsFormalArgs()
        '''«FOR p:angles SEPARATOR','»const «cfg.scalarType»& «p.name»«ENDFOR»'''

        private List<ParametersDeclaration> groups = null
        private Set<ParameterLiteral>       angles = null;
        private IConfigurator cfg = null
        private extension Common common = Common.getInstance()

        interface Root
        {
            public static final String type   = "Parameters"
        }
        static class AngleFunctions
        {
            public static final String type = "AngleFuncValues"
            public static final String field= "trig"
            def public static sinFieldName(ParameterLiteral p) {
                return "sin_" + p.name
            }
            def static cosFieldName(ParameterLiteral p) {
                return "cos_" + p.name
            }
            public static final String updateFuncName = "update";
        }
        static class ParamsGroup
        {
            def static type(ParametersDeclaration gr) { Parameters::structName(gr) }
            def static field(ParametersDeclaration gr) { gr.name }
            def static field(ParameterLiteral p) { p.name }
        }

        def static accessor(ParameterLiteral p) {
            return ParamsGroup.field(Common::getGroup(p)) + "." + ParamsGroup.field(p)
        }
        def static accessor(ParameterLiteral p, String function)
        {
            if( function === null || "".equals(function)) {
                return accessor(p)
            }
            if( "sine".equals(function) ) {
                return AngleFunctions.field + "." + AngleFunctions.sinFieldName(p)
            }
            if( "cosine".equals(function) ) {
                return AngleFunctions.field + "." + AngleFunctions.cosFieldName(p)
            }
            return "// ERROR in iit.dsl.coord.generator.cpp.ParametersStructMeta : unknown function " + function
        }
    }
}

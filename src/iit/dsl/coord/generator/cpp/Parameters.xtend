package iit.dsl.coord.generator.cpp

import iit.dsl.coord.coordTransDsl.ParametersDeclaration
import iit.dsl.coord.coordTransDsl.Model
import java.util.List
import iit.dsl.coord.coordTransDsl.ParameterLiteral

class Parameters {

    def public headerFileContent(Model model, List<String> headers, List<String> enclosingNamespaces, String scalar_t)
    '''
        #ifndef _«model.name.toUpperCase»_PARAMETERS_DEFS_
        #define _«model.name.toUpperCase»_PARAMETERS_DEFS_

        «FOR h : headers»
            #include «h»
        «ENDFOR»

        «FOR ns : enclosingNamespaces»
            namespace «ns» {
        «ENDFOR»

        «FOR gr : model.paramGroups»
            «structDefinition(gr, scalar_t)»

        «ENDFOR»

        «FOR ns : enclosingNamespaces»
            }
        «ENDFOR»
        #endif
    '''

    def public static structName(ParametersDeclaration group) {
        return "Params_" + group.name
    }

    def public static structField(ParameterLiteral p) {
        return p.name
    }

    def protected structDefinition(ParametersDeclaration group, String scalar_t) '''
        struct «structName(group)» {
            «FOR p : group.params»
                «scalar_t» «structField(p)»;
            «ENDFOR»
            «structName(group)»() {
                defaults();
            }
            void defaults() {
                «FOR p : group.params»
                    «p.name» = 0.0; // TODO change this code with an appropriate initial value
                «ENDFOR»
            }
        };
    '''
}
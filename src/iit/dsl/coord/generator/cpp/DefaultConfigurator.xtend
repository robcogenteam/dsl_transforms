package iit.dsl.coord.generator.cpp

import java.util.List
import java.util.ArrayList


import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Variable
import iit.dsl.coord.coordTransDsl.Constant

import iit.dsl.coord.generator.cpp.IConfigurator
import iit.dsl.coord.generator.Utilities$MatrixType


class DefaultVarsAccess implements IVariablesAccess
{
    override valueExpression(Model model, Variable arg) {
        throw new UnsupportedOperationException("Not implemented in dummy/stub configurator")
    }

    override valueExpression(Model model, Variable p, String function) {
        throw new UnsupportedOperationException("Not implemented in dummy/stub configurator")
    }

    override variablesStatusVector_type(Model model) {
        throw new UnsupportedOperationException("Not implemented in dummy/stub configurator")
    }

    override variablesStatusVector_varname(Model model) {
        throw new UnsupportedOperationException("Not implemented in dummy/stub configurator")
    }

}


class DefaultConstantsAccess implements IConstantsAccess
{
    override valueExpression(Model model, Constant c) {
        throw new UnsupportedOperationException("Not implemented in dummy/stub configurator")
    }

    override valueExpression(Model model, Constant c, String function) {
        throw new UnsupportedOperationException("Not implemented in dummy/stub configurator")
    }
}

class DefaultConfigurator implements IConfigurator
{
    override headerFileName(Model model) {
        "transforms"
    }


    override includeDirectives(Model model)
        '''
        #include <iit/rbd/StateDependentMatrix.h>
        '''

    override localTypeName(MatrixType mxtype) {
        switch(mxtype) {
             case Utilities$MatrixType::_6D: "TransformMotion"
             case Utilities$MatrixType::_6D_FORCE: "TransformForce"
             case Utilities$MatrixType::HOMOGENEOUS: "TransformHomogeneous"
             default: throw(new RuntimeException("Unknown MatrixType: " + mxtype))
         }
    }

    override matrixBaseType(Model model, MatrixType mxtype) {
        switch(mxtype) {
             case Utilities$MatrixType::_6D: '''iit::rbd::SpatialTransformBase'''
             case Utilities$MatrixType::_6D_FORCE: '''iit::rbd::SpatialTransformBase'''
             case Utilities$MatrixType::HOMOGENEOUS: '''iit::rbd::HomogeneousTransformBase'''
             default: throw(new RuntimeException("Unknown MatrixType: " + mxtype))
         }
    }


    override List<String> enclosingNamespaces(Model model) {
        val ret = new ArrayList<String>(2)
        ret.add(model.name)
        return ret
    }

    override getMaximaConverterConfigurator() {
        new iit.dsl.coord.generator.maxima.converter.DefaultConfigurator()
    }

    override className(MatrixType mxtype) {
        switch(mxtype) {
             case Utilities$MatrixType::_6D: "MotionTransforms"
             case Utilities$MatrixType::_6D_FORCE: "ForceTransforms"
             case Utilities$MatrixType::HOMOGENEOUS: "HomogeneousTransforms"
             default: throw(new RuntimeException("Unknown MatrixType: " + mxtype))
         }
    }

    override paramsHeaderFileName(Model model) {
        "parameters"
    }



    override getConstantsValueExprGenerator(Model model) {
        return consAccess
    }
    override getVariablesValueExprGenerator(Model model) {
        return varsAccess
    }

    override scalarType() {
        return "double"
    }
    override cosFunctionName() {
        return "std::cos"
    }
    override sinFunctionName() {
        return "std::sin"
    }

    private IVariablesAccess  varsAccess = new DefaultVarsAccess()
    private IConstantsAccess  consAccess = new DefaultConstantsAccess()


}
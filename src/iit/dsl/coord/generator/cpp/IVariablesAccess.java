package iit.dsl.coord.generator.cpp;

import iit.dsl.coord.coordTransDsl.Model;


/**
 * Configuration methods to tune the generated C++ code, as far as the variables
 * of the transforms are concerned.
 *
 * @author Marco Frigerio
 */
public interface IVariablesAccess extends iit.dsl.coord.generator.IVariablesAccess
{
    /**
     * The identifier to be used as the C++ type of the container of the values
     * of the variables.
     *
     * In the generated C++ sources, a method implementing the update of a
     * coordinate-transform will take an argument whose type is a constant
     * reference to the type returned by this function.
     * @param model the set of transforms of interest
     * @return a valid C++ identifier that will be used as a type specifier in
     *    the generated C++ code
     */
    public String variablesStatusVector_type(Model model);
    /**
     * The identifier to be used as the name of the local variable containing
     * the values of the variables.
     *
     * In the generated C++ sources, a method implementing the update of a
     * coordinate-transform will take an argument whose name is the string
     * returned by this function.
     * @param model the set of transforms of interest
     * @return a valid C++ identifier that will be used as the name of a local
     *   variable in the generated C++ code
     */
    public String variablesStatusVector_varname(Model model);

}
package iit.dsl.coord.generator.cpp

import java.util.List
import org.eclipse.xtend2.lib.StringConcatenation

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Transform
import iit.dsl.coord.coordTransDsl.ParameterLiteral
import iit.dsl.coord.coordTransDsl.Variable

import iit.dsl.coord.generator.Common
import iit.dsl.coord.generator.Utilities
import iit.dsl.coord.generator.TransformsInfo
import iit.dsl.coord.generator.maxima.converter.MaximaConverter
import iit.dsl.coord.generator.maxima.converter.Utils
import iit.dsl.coord.generator.maxima.converter.Utils.TransformConversionInfo
import iit.dsl.coord.generator.MatrixGenCommon
import iit.dsl.coord.generator.MaximaDSLReplacements
import iit.dsl.coord.generator.cpp.ContainerClass.ParametersStructMeta

class LocalClasses
{
    new(Model model, List<TransformsInfo$Arguments> info,
        IConfigurator config, boolean using_using  )
    {
        this.model = model
        this.transformsData = info
        this.configurator = config
        this.usingUsing   = using_using
        this.maximaEngine = new MaximaConverter(configurator.maximaConverterConfigurator)
        this.varsAccess   = configurator.getVariablesValueExprGenerator (model)
        this.paramsAccess= new ParametersAccess()
        this.helper = new MatrixGenCommon(
            new CommonGenConfig( configurator.scalarType,
                                 configurator.sinFunctionName,
                                 configurator.cosFunctionName ) )
    }



    def public localClassName(Transform t) '''Type_«t.name»'''
    def public updateFunctionName() '''update'''


    def private String containerClassQualifier(Model model, Utilities$MatrixType mxtype)
    {
        val qual = if(usingUsing) "" else ContainerClass.nsQualifier(configurator,model) + "::"
        return qual + configurator.className(mxtype)
    }


    def public localType(Utilities$MatrixType mxtype) {
        return configurator.localTypeName(mxtype)
    }


    def public localClassesDeclaration(Utilities$MatrixType mxtype)
    '''
        «FOR info : transformsData»
            «val t = info.transform»
            struct «localClassName(t)» : public «localType(mxtype)»<«localClassName(t)»>
            {
                «localClassName(t)»(«ctorArgs(info)»);
                const «localClassName(t)»& «updateFunctionName»(const «ContainerClass.varsvector_t»&);
                «IF info.parametric»
                    protected:
                        const Parameters& «member_params»;
                «ENDIF»
            };

        «ENDFOR»
    '''

    def public localClassesDefinitions(Utilities$MatrixType mxtype)
    {
        val builder = new StringConcatenation();
        val details = Utils.getConversionInfo(model, transformsData, mxtype, maximaEngine)

        this.replaceSpecs = helper.makeMaximaDSLReplacements(model,
            this.varsAccess,
            paramsAccess,
            configurator.getConstantsValueExprGenerator(model) )

        for( info : details )
        {
            builder.append(constructorCode (info))
            builder.newLine()builder.newLine()
            builder.append(updateMethodCode(info))
        }

        return builder
    }


    def public ctorArgs(TransformsInfo$Arguments info)
    {
        if(info.parametric) {
            return "const Parameters& refParams"
        }
        return ""
    }


    def private static parameterValueAccessor(ParameterLiteral p, String function)
    {
        member_params + "." + ParametersStructMeta.accessor(p, function)
    }

    def private static member_params() {
        return "params"
    }


    def private constructorCode(TransformConversionInfo info)
    {
        val transform = info.basic.transform
        val classname = localClassName(transform)
        return
        '''
        «containerClassQualifier(model, info.mxtype)»::«classname»::«classname»(«ctorArgs(info.basic)»)
            «IF info.basic.parametric» : params(refParams)«ENDIF»
        {
            «helper.matrixInitCode(info.layers, info.basic.constants, replaceSpecs)»
        }'''
    }


    def private updateMethodCode(TransformConversionInfo info)
    {
        val fqClass = containerClassQualifier(model, info.mxtype) + "::" +
                      localClassName(info.basic.transform)
        return
        '''
        const «fqClass»& «fqClass»::«updateFunctionName»(const «ContainerClass.varsvector_t»& «varsAccess.variablesStatusVector_varname(model)»)
        {
            «helper.matrixUpdateCode(model, info, replaceSpecs)»
            return *this;
        }
        '''
    }


    private extension Common common = new Common()
    private MaximaConverter maximaEngine = null
    private IConfigurator configurator = null
    private boolean usingUsing = false

    private Model model = null
    private List<TransformsInfo$Arguments> transformsData = null

    private IVariablesAccess      varsAccess   = null
    private IParametersAccess     paramsAccess = null
    private MaximaDSLReplacements replaceSpecs = null

    private MatrixGenCommon helper = null


    private static class ParametersAccess implements IParametersAccess
    {
        override valueExpression(Model model, ParameterLiteral p) {
            return LocalClasses.parameterValueAccessor(p, "");
        }

        override valueExpression(Model model, ParameterLiteral p, String function) {
            return LocalClasses.parameterValueAccessor(p, function);
        }
    }

    public static class CommonGenConfig implements MatrixGenCommon.IConfigurator
    {
        public new (String scalar, String sinName, String cosName) {
            this.scalar_t = scalar
            this.sin = sinName
            this.cos = cosName
        }

        override matrixAssignment(int row, int col, String valueExpression) {
            return "(*this)(" + row + "," + col + ") = " + valueExpression + ";"
        }

        override singleLineComment(String comment) {
            return "// " + comment
        }

        override sinLocalVarDefinition(Variable v, String valueExpression) {
            return '''«scalar_t» «sinLocalVarName(v)»  = «sin»( «valueExpression» );'''
        }

        override cosLocalVarDefinition(Variable v, String valueExpression) {
            return '''«scalar_t» «cosLocalVarName(v)»  = «cos»( «valueExpression» );'''
        }

        override sinLocalVarIdentifier(Variable v) {
            return sinLocalVarName(v)
        }

        override cosLocalVarIdentifier(Variable v) {
            return cosLocalVarName(v)
        }

        def public static String sinLocalVarName(Variable arg) {
           return "sin_" + arg.varname
        }
        def public static String cosLocalVarName(Variable arg) {
            return "cos_" + arg.varname
        }

        private final String scalar_t
        private final String sin
        private final String cos
    }
}

package iit.dsl.coord.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import iit.dsl.coord.CoordTransDslStandaloneSetupGenerated;
import iit.dsl.coord.coordTransDsl.Model;

import com.google.inject.Injector;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.util.StringInputStream;

/**
 * This class loads the runtime of the Transforms-DSL (package iit.dsl.coord...)
 * and allows to retrieve the corresponding models, given a string with the
 * text of a compliant document (ie the text model)
 *
 * @author Marco Frigerio
 */
public class DSLAccessor
{
    private static Injector injector =
            new CoordTransDslStandaloneSetupGenerated().createInjectorAndDoEMFRegistration();
    private Resource resource = null;
    private XtextResourceSet set = null;

    public DSLAccessor()
    {
        set = injector.getInstance(XtextResourceSet.class);
        set.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
    }

    public Model getModel(String model) throws IOException
    {
        URI uri = URI.createURI("dummy:/"+Long.toString(System.nanoTime())+".ctdsl");
        resource = set.createResource(uri);
        InputStream in = new StringInputStream(model);
        resource.load(in, set.getLoadOptions());
        return getModel(uri);
    }

    private Model getModel(final URI uri)
    {
        resource = set.getResource(uri, true);
        List<Resource.Diagnostic> errors = resource.getErrors();
        if(errors.size() > 0) {
            StringBuffer msg = new StringBuffer();
            msg.append("Errors while loading a document of the Transforms-DSL ("
                        + uri.toString() + "):\n");
            for(Resource.Diagnostic err : errors) {
                msg.append("\n\t " + err.getMessage() + "\n");
            }
            throw new RuntimeException(msg.toString());
        }
        return (Model)resource.getContents().get(0);
    }

}
